# 2017.04.19 13:55:40 CDT
#Embedded file name: C:\Users\keega\uflow\gui_toolkit.py
from pyglet.gl import *
import pyglet as pg
try:
    from functools import reduce
except:
    pass

def vec(*args):
    return (GLfloat * len(args))(*args)


def cross(a, b):
    return a[0] * b[1] - b[0] * a[1]


def dot(a, b):
    return a[0] * b[0] + a[1] * b[1]


class Container(object):
    widgets = []

    def __init__(self, *args, **kwargs):
        super(Container, self).__init__()
        self.parent = kwargs.get('parent', None)
        self.children = []
        self.position = kwargs.get('position', (0, 0))
        self.window = kwargs.get('window', None)
        self.fill = kwargs.get('fill', vec(0, 0, 0, 0))
        self.hover_fill = kwargs.get('hover_fill', None)
        self.width = kwargs.get('width', -1)
        self.height = kwargs.get('height', -1)
        self.fill_w = kwargs.get('fill_w', False)
        self.fill_h = kwargs.get('fill_h', False)
        self.double_click_duration = kwargs.get('double_click_duration', 0.4)
        if type(self.width) == str and self.width.endswith('%'):
            self.width = float(self.width[:-1]) / 100.0
            self.fill_w = True
        if type(self.height) == str and self.height.endswith('%'):
            self.height = float(self.height[:-1]) / 100.0
            self.fill_h = True
        self.scale = kwargs.get('scale', (1, 1))
        self.anchor = kwargs.get('anchor', (0, 0))
        self.focus_on_click = kwargs.get('focus_on_click', True)
        self.focus_on_mouseover = kwargs.get('focus_on_mouseover', False)
        self.focused = False
        self.focused_child = None
        self.parent = None
        self.mouse_in = False
        self.enabled = True
        Container.widgets.append(self)
        self.update()
        self.double_clicking = False
        self.double_click_time = 10000
        self.hidden = False

    def hide(self):
        self.hidden = True

    def unhide(self):
        self.hidden = False

    def disable(self):
        self.enabled = False

    def enable(self):
        self.enabled = True

    def _draw(self):
        if not self.hidden:
            glPushMatrix()
            x = self.position[0]
            y = self.position[1]
            if self.position[0] < 0:
                x = self.parent._width + self.position[0]
            if self.position[1] < 0:
                y = self.parent._height + self.position[1]
            glTranslatef(x, y, 0)
            glScalef(self.scale[0], self.scale[1], 1)
            glTranslatef(-self.anchor[0] * self._width, -self.anchor[1] * self._height, 0)
            self.draw()
            for c in self.children:
                c._draw()

            glPopMatrix()

    def _tick(self, dt):
        if self.enabled:
            if self.double_clicking:
                self.double_click_time += dt
                if self.double_click_time > self.double_click_duration:
                    self.double_clicking = False
            self.tick(dt)
            for c in self.children:
                c._tick(dt)

    def tick(self, dt):
        pass

    def draw(self):
        pass

    def get_local_origin(self):
        x = self.position[0]
        y = self.position[1]
        if self.position[0] < 0:
            x = self.parent._width + self.position[0]
        if self.position[1] < 0:
            y = self.parent._height + self.position[1]
        x -= self.anchor[0] * self._width
        y -= self.anchor[1] * self._height
        return (x, y)

    def get_global_origin(self):
        pos = self.get_local_origin()
        newp = self
        while newp.parent is not None:
            newp = newp.parent
            p2 = newp.get_local_origin()
            pos = (pos[0] + p2[0], pos[1] + p2[1])

        return pos

    def get_bounding_box(self):
        x = self.position[0]
        y = self.position[1]
        if self.position[0] < 0:
            x = self.parent._width + self.position[0]
        if self.position[1] < 0:
            y = self.parent._height + self.position[1]
        return (x - self.anchor[0] * self._width,
         y - self.anchor[1] * self._height,
         self._width,
         self._height)

    def contains_point(self, point):
        bb = self.get_bounding_box()
        return point[0] > bb[0] and point[0] < bb[0] + bb[2] * self.scale[0] and point[1] > bb[1] and point[1] < bb[1] + bb[3] * self.scale[1]

    def unfocus(self):
        self.focused = False
        if self.focused_child is not None:
            self.focused_child.unfocus()
            self.focused_child = None
        else:
            self.on_unfocus()

    def get_root(self):
        target = None
        ntarget = self
        while ntarget is not None:
            target = ntarget
            ntarget = target.parent

        return target

    def _focus(self):
        self.focused = True
        if self.parent is not None:
            self.parent.focused_child = self
            self.parent._focus()

    def focus(self):
        newp = self
        while newp.parent is not None and not newp.focused:
            newp = newp.parent

        if newp.focused_child is not None:
            newp.focused_child.unfocus()
            newp.focused_child = None
        self.on_focus()
        self._focus()

    def on_focus(self):
        pass

    def on_unfocus(self):
        pass

    def _on_mouse_press(self, mx, my, buttons, mods):
        if self.enabled:
            ax, ay = self.get_local_origin()
            nx = mx - ax
            ny = my - ay
            nx /= self.scale[0]
            ny /= self.scale[1]
            handled = False
            if len(self.children) > 0:
                for c in self.children[::-1]:
                    if c.contains_point((nx, ny)):
                        handled = c._on_mouse_press(nx, ny, buttons, mods)
                        if handled:
                            break

            if not handled:
                if self.focus_on_click:
                    self.focus()
                if not self.double_clicking:
                    self.double_clicking = True
                    self.double_click_time = 0
                    return self.on_mouse_press(nx, ny, buttons, mods)
                res = False
                if self.double_click_time < self.double_click_duration:
                    self.double_clicking = False
                    res = self.on_double_click(nx, ny, buttons, mods)
                if not res:
                    return self.on_mouse_press(nx, ny, buttons, mods)
                else:
                    return True
            else:
                return True
        else:
            return False

    def on_double_click(self, nx, ny, buttons, mods):
        return False

    def _on_mouse_motion(self, mx, my, dx, dy):
        if self.enabled:
            ax, ay = self.get_local_origin()
            nx = mx - ax
            ny = my - ay
            nx /= self.scale[0]
            ny /= self.scale[1]
            handled = False
            self.double_clicking = False
            if not self.mouse_in:
                self.mouse_in = True
                self.on_mouse_over(mx, my)
            for c in self.children[::-1]:
                if c.contains_point((nx, ny)):
                    handled = handled or c._on_mouse_motion(nx, ny, dx, dy)
                elif c.mouse_in:
                    c._on_mouse_out()

            if not handled:
                if self.focus_on_mouseover:
                    self.focus()
                return self.on_mouse_motion(nx, ny, dx, dy)
            else:
                return True
        else:
            return False

    def on_mouse_over(self, mx, my):
        pass

    def _on_mouse_out(self):
        self.mouse_in = False
        for c in self.children[::-1]:
            c._on_mouse_out()

        self.on_mouse_out()

    def on_mouse_out(self):
        pass

    def _on_mouse_scroll(self, mx, my, scroll_x, scroll_y):
        if self.enabled:
            ax, ay = self.get_local_origin()
            nx = mx - ax
            ny = my - ay
            nx /= self.scale[0]
            ny /= self.scale[1]
            if len(self.children) == 0 or not reduce(lambda x, y: x or y, map(lambda x: x.contains_point((nx, ny)) and x._on_mouse_scroll(nx, ny, scroll_x, scroll_y), self.children[::-1])):
                return self.on_mouse_scroll(nx, ny, scroll_x, scroll_y)
            else:
                return True
        else:
            return False

    def _on_mouse_drag(self, mx, my, dx, dy, buttons, mods):
        if self.enabled:
            ax, ay = self.get_local_origin()
            nx = mx - ax
            ny = my - ay
            nx /= self.scale[0]
            ny /= self.scale[1]
            ndx = dx / self.scale[0]
            ndy = dy / self.scale[1]
            if self.focused_child is not None:
                if not self.focused_child._on_mouse_drag(nx, ny, ndx, ndy, buttons, mods):
                    return self.on_mouse_drag(nx, ny, dx, dy, buttons, mods)
                else:
                    return True
            else:
                return self.on_mouse_drag(nx, ny, dx, dy, buttons, mods)
        else:
            return False

    def _on_mouse_release(self, mx, my, buttons, mods):
        if self.enabled:
            ax, ay = self.get_local_origin()
            nx = mx - ax
            ny = my - ay
            nx /= self.scale[0]
            ny /= self.scale[1]
            if self.focused_child is not None:
                if not self.focused_child._on_mouse_release(nx, ny, buttons, mods):
                    return self.on_mouse_release(nx, ny, buttons, mods)
                else:
                    return True
            elif self.focused:
                return self.on_mouse_release(nx, ny, buttons, mods)
            else:
                return False

        else:
            return False

    def on_mouse_press(self, x, y, buttons, mods):
        return False

    def on_mouse_motion(self, mx, my, dx, dy):
        return False

    def on_mouse_drag(self, x, y, dx, dy, buttons, mods):
        return False

    def on_mouse_release(self, x, y, buttons, mods):
        return False

    def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
        return False

    def update(self):
        self._width = 0
        self._height = 0
        if self.width == -1 and len(self.children) > 0:
            self._width = max(map(lambda x: x._width, self.children))
        else:
            self._width = self.width
        if self.height == -1 and len(self.children) > 0:
            self._height = max(map(lambda x: x._height, self.children))
        else:
            self._height = self.height
        if self.fill_w == True and self.parent is not None:
            self._width = self.parent._width * self.width
        if self.fill_h == True and self.parent is not None:
            self._height = self.parent._height * self.height
        for child in self.children:
            child.update()

    def addChild(self, child):
        self.children.append(child)
        child.parent = self
        self.update()
        child.update()

    def removeChild(self, child):
        self.children.remove(child)
        child.parent = None
        self.update()
        child.update()


class SlideContainer(Container):

    def __init__(self, *args, **kwargs):
        self.decorator = kwargs.get('decorator', 't')
        self.continuous = kwargs.get('continuous', True)
        self.legal_values = kwargs.get('legal_values', (0, 0.2, 0.5, 0.9, 1))
        self.range = kwargs.get('range', (0, 1))
        self.value = kwargs.get('value', 0.5)
        self.target = self.value
        self.fill = kwargs.get('fill', vec(0.9, 0.9, 0.9, 0.8))
        self.start_dragpos = (0, 0)
        super(SlideContainer, self).__init__(*args, **kwargs)
        kwargs['position'] = (0, 0)
        kwargs['anchor'] = (0.5, 0)
        kwargs['width'] = 15
        kwargs['height'] = 15
        kwargs['pos_extents'] = (8, self._width - 8)
        self.pos_extents = kwargs['pos_extents']
        self.slider = Slider(*args, **kwargs)
        self.addChild(self.slider)

    def on_mouse_press(self, x, y, buttons, mods):
        newval = (x - self.pos_extents[0]) * (self.range[1] - self.range[0]) / (self.pos_extents[1] - self.pos_extents[0]) + self.range[0]
        if newval < self.range[0]:
            newval = self.range[0]
        elif newval > self.range[1]:
            newval = self.range[1]
        self.slider.set_value(newval)
        return True

    def on_mouse_drag(self, x, y, dx, dy, buttons, mods):
        newval = (x - self.pos_extents[0]) * (self.range[1] - self.range[0]) / (self.pos_extents[1] - self.pos_extents[0]) + self.range[0]
        if newval < self.range[0]:
            newval = self.range[0]
        elif newval > self.range[1]:
            newval = self.range[1]
        self.slider.set_value(newval)
        return True

    def update(self):
        super(SlideContainer, self).update()
        if not self.continuous:
            self.range = (min(self.legal_values), max(self.legal_values))

    def draw(self):
        glColor4f(1, 1, 1, 1)
        glBegin(GL_QUADS)
        for v in self.legal_values:
            x0 = (v - self.range[0]) / (self.range[1] - self.range[0]) * (self.pos_extents[1] - self.pos_extents[0]) + self.pos_extents[0]
            w = 1
            h = 5
            vo = 17
            glVertex2f(x0, vo)
            glVertex2f(x0, vo + h)
            glVertex2f(x0 + w, vo + h)
            glVertex2f(x0 + w, vo)

        glEnd()


class ResizeHandle(Container):

    def __init__(self, *args, **kwargs):
        super(ResizeHandle, self).__init__(*args, **kwargs)
        self.start_drag = (0, 0)

    def on_mouse_drag(self, x, y, dx, dy, buttons, mods):
        root = self.get_root()
        size = root.window.get_size()
        ndx = x - self.start_drag[0]
        ndy = y - self.start_drag[1]
        root.window.set_size(size[0] + ndx, size[1] - ndy)

    def on_mouse_press(self, x, y, buttons, mods):
        self.start_drag = (x, y)
        return True

# To Do: Add entry container for simply inputting values
# class Entry(Container):
#
#     def __init__(self, *args, **kwargs):



class Slider(Container):

    def __init__(self, *args, **kwargs):
        super(Slider, self).__init__(*args, **kwargs)
        self.decorator = kwargs.get('decorator', 't')
        self.pos_extents = kwargs.get('pos_extents', (20, 80))
        self.continuous = kwargs.get('continuous', True)
        self.legal_values = kwargs.get('legal_values', (0, 0.2, 0.5, 0.9, 1))
        self.range = kwargs.get('range', (0, 1))
        if not self.continuous:
            self.range = (min(self.legal_values), max(self.legal_values))
        self.value = kwargs.get('value', 0.5)
        self.target = self.value
        self.start_dragpos = (0, 0)
        self.fill = kwargs.get('fill', vec(0.9, 0.9, 0.9, 0.8))
        self.anchor = (0.5, 0)
        newx = float(self.value - self.range[0]) / (self.range[1] - self.range[0]) * (self.pos_extents[1] - self.pos_extents[0]) + self.pos_extents[0]
        self.position = (newx, self.position[1])

    def on_mouse_press(self, x, y, buttons, mods):
        self.start_dragpos = (x, y)
        return True

    def on_mouse_drag(self, x, y, dx, dy, buttons, mods):
        if buttons & pg.window.mouse.LEFT:
            newval = float(self.position[0] + x - self.start_dragpos[0] - self.pos_extents[0]) / (self.pos_extents[1] - self.pos_extents[0]) * (self.range[1] - self.range[0]) + self.range[0]
            if newval < self.range[0]:
                newval = self.range[0]
            elif newval > self.range[1]:
                newval = self.range[1]
            self.target = newval
            if not self.continuous:
                newval = min(self.legal_values, key=lambda x: abs(x - newval))
            if self.value != newval:
                self.value = newval
                newx = float(newval - self.range[0]) / (self.range[1] - self.range[0]) * (self.pos_extents[1] - self.pos_extents[0]) + self.pos_extents[0]
                self.position = (newx, self.position[1])
                self.on_value_changed(self)
            return True
        return False

    def set_value(self, newval):
        self.target = newval
        if not self.continuous:
            self.range = (min(self.legal_values), max(self.legal_values))
            newval = min(self.legal_values, key=lambda x: abs(x - newval))
        if self.value != newval:
            self.value = newval
            newx = float(newval - self.range[0]) / (self.range[1] - self.range[0]) * (self.pos_extents[1] - self.pos_extents[0]) + self.pos_extents[0]
            self.position = (newx, self.position[1])
            self.on_value_changed(self)

    def on_value_changed(self, slider):
        pass

    def draw(self):
        glEnable(GL_POLYGON_SMOOTH)
        glColor4f(*self.fill)
        glBegin(GL_TRIANGLES)
        glVertex2f(0, 0)
        glVertex2f(self._width, 0)
        glVertex2f(self._width / 2.0, self._height)
        glEnd()
        glDisable(GL_POLYGON_SMOOTH)


class Window(Container):

    def __init__(self, *args, **kwargs):
        super(Window, self).__init__(*args, **kwargs)
        self.draggable = kwargs.get('draggable', False)
        self.resizeable = kwargs.get('resizeable', False)
        self.fill = kwargs.get('fill', vec(0.2, 0.2, 0.2, 0.8))
        self.hover_fill = kwargs.get('hover_fill', None)
        self.title = kwargs.get('title', '')
        self.start_dragpos = (0, 0)
        self.hovering = False

    def draw(self):
        col = self.fill
        if self.hovering and self.hover_fill is not None:
            col = self.hover_fill
        glColor4f(*col)
        glBegin(GL_QUADS)
        glVertex2f(0, 0)
        glVertex2f(self._width, 0)
        glVertex2f(self._width, self._height)
        glVertex2f(0, self._height)
        glEnd()

    def on_mouse_over(self, mx, my):
        self.hovering = True
        return False

    def on_mouse_out(self):
        self.hovering = False
        return False

    def on_mouse_press(self, x, y, buttons, mods):
        self.start_dragpos = (x, y)
        return True

    def on_mouse_motion(self, mx, my, dx, dy):
        return False

    def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
        if self.resizeable:
            self.scale = (self.scale[0] * (1 + 0.05 * scroll_y), self.scale[1] * (1 + 0.05 * scroll_y))
            return True
        else:
            return False

    def on_mouse_drag(self, x, y, dx, dy, buttons, mods):
        if self.focused and self.draggable and self.focused_child is None and buttons & pg.window.mouse.LEFT:
            newx = -self.start_dragpos[0] + x
            newy = -self.start_dragpos[1] + y
            self.position = (self.position[0] + newx, self.position[1] + newy)
            return True
        return True


class Label(Container):

    def __init__(self, *args, **kwargs):
        super(Label, self).__init__(*args, **kwargs)
        self.color = kwargs.get('color', (1, 1, 1, 1))
        self.text = kwargs.get('text', 'Label')
        self.labelopts = kwargs.get('labelopts', {})
        self.pglabel = pg.text.HTMLLabel(self.text, **self.labelopts)
        self._text = self.text
        self.pglabel.color = tuple(map(lambda x: int(x * 255), self.color))
        self._color = self.color

    def draw(self):
        if self._color != self.color:
            self._color = self.color
            self.pglabel.color = tuple(map(lambda x: int(x * 255), self.color))
        if self._text != self.text:
            self.pglabel.text = self.text
            self._text = self.text
            self.pglabel.color = tuple(map(lambda x: int(x * 255), self.color))
        self.pglabel.draw()


class Button(Container):

    def __init__(self, *args, **kwargs):
        super(Button, self).__init__(*args, **kwargs)
        self.fill = kwargs.get('fill', vec(0.3, 0.3, 0.3, 0.8))
        self.hover_fill = kwargs.get('hover_fill', vec(0.6, 0.6, 0.6, 0.8))
        self.pressed_fill = kwargs.get('pressed_fill', vec(1, 1, 0.5, 1))
        self.pressing = False
        self.hovering = False

    def on_mouse_press(self, x, y, buttons, mods):
        self.pressing = True
        return True

    def on_mouse_release(self, x, y, buttons, mods):
        if self.pressing:
            self.pressing = False
            self.on_button_press()
            return True
        return False

    def on_mouse_drag(self, x, y, dx, dy, buttons, mods):
        if not x < self._width or not y < self._height:
            self.pressing = False
        else:
            self.pressing = True
        return True

    def on_button_press(self):
        pass

    def draw(self):
        glBegin(GL_QUADS)
        col = self.fill
        if self.hovering:
            col = self.hover_fill
        if self.pressing:
            col = self.pressed_fill
        glColor4f(*col)
        glVertex2f(0, 0)
        glVertex2f(self._width, 0)
        glVertex2f(self._width, self._height)
        glVertex2f(0, self._height)
        glEnd()

    def on_mouse_over(self, mx, my):
        self.hovering = True
        return True

    def on_mouse_out(self):
        self.hovering = False
        return True


def main():
    return 0


if __name__ == '__main__':
    main()
