#ifdef GL_ES
precision highp float;
#endif

uniform vec2 uv_scale;
uniform vec2 uv_offset;
uniform sampler2D disp0;
uniform sampler2D disp1;
uniform float lv;
uniform float rv;
uniform float fac;
uniform float yw;
uniform float zw;


void main(void)
{
    gl_TexCoord[0] = gl_MultiTexCoord0;
    vec2 p = gl_MultiTexCoord0.st*uv_scale + uv_offset;

    vec2 uv0 = texture2D(disp0, p).xy;
    
    vec2 uv1 =texture2D(disp1, p).xy;
    vec2 uv = fac*uv1 + (1.-fac)*uv0;
    
    gl_Position = gl_ModelViewProjectionMatrix * (gl_Vertex + vec4(uv/vec2(yw, zw)/uv_scale, 0., 0.));
}
