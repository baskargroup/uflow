#!/usr/bin/env python

from scipy.io import loadmat
#from scipy.interpolate import interp2d
from pylab import *

data = loadmat("P_1_fine_7.txt")

def pillar_transform(pt):
    a = round((pt[0]+99.75e-6)/0.25e-6)
    b = round(pt[1]/.25e-6)
    return (data["P"][a,b,1,4], data["P"][a,b,1,5])
        

def run():
	px = arange(-10, 10,.5)*1e-6
	py = arange(0, 20, .5)*1e-6

	points = [(i,k) for i in px for k in py]

	subplot(3,3,1, xlim=[-50e-6,50e-6], ylim=[0,20e-6])
	plot(*zip(*points), marker='.', ls='')
	for i in range(1,9):
	    subplot(3,3,i+1, xlim=[-50e-6,50e-6], ylim=[0,20e-6])
	    points = map(pillar_transform, points)
	    plot(*zip(*points), marker='.', ls='')
