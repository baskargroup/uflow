'''
Created on Jun 26, 2014

@author: keegan
'''

from pyglet.gl import *
import numpy as np
from ctypes import byref
import struct

class FBO(object):
    '''
    A class that wraps OpenGL framebuffer objects in an easy-to-use interface.
    '''
    def __init__(self, *args, **kwargs):
        self._fbo = GLuint()
        glGenFramebuffersEXT(1, byref(self._fbo))
        self.__attachments = (GL_COLOR_ATTACHMENT0_EXT, )
        self.__pushed_matrix = False

    
    def compatible(self, texture):
        self.bind(texture)
        status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT)
        self.unbind()
        return status
        
    def bind(self, target = None, attachment = 0, level=0):
        self.__pushed_matrix = False
        if target is not None:
            try:
                t = target._texindex.value
            except:
                t = target
            glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, self._fbo)
            glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, self.__attachments[attachment], GL_TEXTURE_2D, t, level)

            glMatrixMode(GL_PROJECTION)
            glPushMatrix()
            glLoadIdentity()
            glOrtho(0,1,0,1,-1,1)
            d = target.get_dimensions()
            glPushAttrib(GL_VIEWPORT_BIT)
            glViewport(0,0,d[0], d[1])
            glMatrixMode(GL_MODELVIEW)
            glPushMatrix()
            glLoadIdentity()
            self.__pushed_matrix = True
    
    def unbind(self):
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)
        if self.__pushed_matrix:
            glMatrixMode(GL_PROJECTION)
            glPopMatrix()
            glMatrixMode(GL_MODELVIEW)
            glPopMatrix()
            self.__pushed_matrix = False
            glPopAttrib()
        

class Texture(object):
    '''
    A class that wraps typical OpenGL texture functionality in an easy-to-use
    interface.
    '''
    @staticmethod
    def from_file(file_like, name = ""):
        header = struct.unpack('12sIIIII', file_like.read(32))
        if header[0] != b'KEEGFLT32IMG':
            raise TypeError("File is not a valid 32-bit float texture.")
            return
        d = np.fromstring(file_like.read(), dtype=np.float32, count=header[1]*header[2]*header[3]).reshape(header[2], header[3],header[1])
        file_like.close()
        if header[1] == 2:
            header = (header[0], 3, header[2], header[3])
            d = np.append(d,np.zeros((header[2], header[3],1), dtype=np.float32),axis=2)
            #d = d.flatten()
        return Texture(d, channels = header[1], dims = (header[2], header[3]), name = name)
        
    def __init__(self, *args, **kwargs):
        self._num = kwargs.get("n", 1)
        self.type = kwargs.get("target", '2D')
        self._interpolation = kwargs.get("interpolation", "linear")
        self._wrap = kwargs.get("wrap", "clamp")
        self._levels = kwargs.get("levels", 0)
        self._format = kwargs.get("channels", 4)
        self._datatype = kwargs.get('type', None)
        self._channels = self._format
        self._dims = kwargs.get("dims", (256, 256))
        self._clamp = kwargs.get("clamp", False)
        self._bitdepth = kwargs.get("bits", 32)
        self._name = kwargs.get("name", "")
        
        
        self._bordercolor = kwargs.get("border", (0, 0, 0, 1))

        self.__GL_TEXTURE = (GL_TEXTURE0, GL_TEXTURE1, GL_TEXTURE2, GL_TEXTURE3)
        
        self._mipmapping = self._levels > 0
        
        if len(args) > 0:
            #user passed in some data
            #let's figure out what it is
            d = args[0]
            if self._num > 1:
                d = args[0][0]
                
            flttype = (GLfloat * (self._dims[0]*self._dims[1]*self._channels))
            inttype = (GLint * (self._dims[0]*self._dims[1]*self._channels))
            uinttype = (GLuint * (self._dims[0]*self._dims[1]*self._channels))
            if type(d) == np.ndarray:
                if d.dtype == np.float32:
                    self._datatype = GL_FLOAT
                    self._zeroes = flttype(0)
                    self._extendedtype = flttype
                    self._data = d.tostring()
            elif type(d) == np.array:
                if d.dtype == np.float32:
                    self._datatype = GL_FLOAT
                    self._zeroes = flttype(0)
                    self._extendedtype = flttype
                    self._data = d.tostring()
            elif type(d) == flttype:
                self._datatype = GL_FLOAT
                self._zeroes = flttype(0)
                self._extendedtype = flttype
                self._data = d
            elif type(d) == inttype:
                self._datatype = GL_INT
                self._zeroes = inttype(0)
                
                self._extendedtype = inttype
                self._data = d
            elif type(d) == uinttype:
                self._datatype = GL_UNSIGNED_INT
                self._zeroes = uinttype(0)
                
                self._extendedtype = uinttype
                self._data = d
        else:
            self._datatype = GL_FLOAT
            self._extendedtype = (GLfloat * (self._dims[0]*self._dims[1]*self._channels))
            self._zeroes = self._extendedtype(0)
            self._data = None
                
        
        if self._format == 4:
            self._format = GL_RGBA
            if self._bitdepth == 32 and not self._clamp:
                self._internalformat = GL_RGBA32F_ARB
            elif self._bitdepth == 8 and self._clamp:
                self._internalformat = GL_RGBA
            else:
                #TBD
                raise NotImplementedError
        elif self._format == 3:
            self._format = GL_RGB
            if self._bitdepth == 32 and not self._clamp:
                self._internalformat = GL_RGB32F_ARB
            elif self._bitdepth == 8 and self._clamp:
                self._internalformat = GL_RGB
            else:
                #TBD
                raise NotImplementedError
        elif self._format == 2:
            raise NotImplementedError
        elif self._format == 1:
            self._format = GL_LUMINANCE
            if self._bitdepth == 32 and not self._clamp:
                self._internalformat = GL_RGB32F_ARB
            elif self._bitdepth == 8 and self._clamp:
                self._internalformat = GL_RGB
            else:
                #TBD
                raise NotImplementedError
        
        if self.type == '2D':
            self.type = GL_TEXTURE_2D
            self._imgcmd = glTexImage2D
        elif self.type == '1D':
            self.type = GL_TEXTURE_1D
            self._imgcmd = glTexImage1D
        elif self.type == '3D':
            self.type = GL_TEXTURE_3D
            self._imgcmd = glTexImage3D
        
        if self._interpolation == 'linear':
            self._magterp = GL_LINEAR
            if self._mipmapping:
                self._minterp = GL_LINEAR_MIPMAP_LINEAR
            else:
                self._minterp = GL_LINEAR
        elif self._interpolation == "nearest":
            self._magterp = GL_NEAREST
            if self._mipmapping:
                self._minterp = GL_NEAREST_MIPMAP_NEAREST
            else:
                self._minterp = GL_NEAREST
        
        if self._wrap == 'clamp':
            self._wrap = GL_CLAMP_TO_EDGE
        elif self._wrap == 'repeat':
            self._wrap = GL_REPEAT
        elif self._wrap == 'mirrored':
            self._wrap = GL_MIRRORED_REPEAT
        elif self._wrap == 'border':
            self._wrap = GL_CLAMP_TO_BORDER
        
        self._texindex = GLuint()
        glGenTextures(self._num, byref(self._texindex))
        
        for i in range(self._num):
            glBindTexture(self.type, self._texindex.value + i)
            glTexParameteri(self.type, GL_TEXTURE_MIN_FILTER, self._minterp)
            glTexParameteri(self.type, GL_TEXTURE_MAG_FILTER, self._magterp)
            
            glTexParameteri(self.type, GL_TEXTURE_WRAP_S, self._wrap)
            glTexParameteri(self.type, GL_TEXTURE_WRAP_T, self._wrap)
            
            self._imgcmd(self.type, self._levels, self._internalformat, self._dims[0], 
                         self._dims[1], 0, self._format, self._datatype, self._data)
        
        del self._zeroes, self._data
        
    def clear(self, n = 0):
        glBindTexture(self.type, self._texindex.value + n)
        self._imgcmd(self.type, self._levels, self._internalformat, self._dims[0], 
                         self._dims[1], 0, self._format, self._datatype, self._extendedtype(0))
        glBindTexture(self.type, 0)
        
    def write_pixels(self, pixels, i=0, format = None,datatype = None, offset = (0,0), level=0, dims = None):
        if datatype is None:
            datatype = self._datatype
        if format is None:
            format = self._format
        if dims is None:
            dims = self._dims
        glBindTexture(self.type, self._texindex.value + i)
        glTexSubImage2D(self.type, level, offset[0], offset[1], 
                         dims[0], dims[1], format, datatype, pixels)
        glBindTexture(self.type, 0)
        
    def read_pixels(self):
        output = (GLfloat * (self._channels*self._dims[0]*self._dims[1]))(0)
        
        #Get pixels from GPU
        glBindTexture(GL_TEXTURE_2D, self._texindex.value)
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, output)
        
        #Cast into numpy array
        res = np.frombuffer(output, dtype=np.float32).reshape(self._dims[1], self._dims[0], self._channels)
        return res
    def bind(self, n = 0, tex = None):
        if tex is not None:
            glActiveTexture(self.__GL_TEXTURE[tex])
        glBindTexture(self.type, self._texindex.value + n)
        
    def unbind(self, n = 0, tex = None):
        if tex is not None:
            glActiveTexture(self.__GL_TEXTURE[tex])
        glBindTexture(self.type, 0)
        
    def __del__(self):
        self.release()
        
    def get_dimensions(self):
        return self._dims
        
    def release(self):
        #TBD: free texture memory
        glDeleteTextures(self._num, self._texindex)