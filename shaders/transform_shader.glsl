#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D prev;
uniform sampler2D disp;
uniform bool xmirror;
uniform float yw;
uniform float zw;


void main(void)
{
    vec2 uv = gl_TexCoord[0].xy;

	vec2 displ = texture2D(prev, uv).xy;
	vec2 uv2 = uv + displ.xy/vec2(yw, zw);
	
	vec4 newpos = vec4(displ, 0., 0.);
    if(!xmirror)
    {
		newpos += texture2D(disp,uv2);
	}
	else
	{
		uv2.x = 1.-uv2.x;
		vec4 d = texture2D(disp,uv2);
		d.x = -d.x;
		newpos += d;
	}

	//col = vec4(1., 0., 0., 1.);
    gl_FragColor = newpos;
}
