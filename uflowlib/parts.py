import numpy as np

class Part(object):
    '''Abstract class for microfluidic parts. Parts can be assembled
    together using attachment points on the part object. This class is
    subclassed in order to make usable parts for assembly.
    
    Each part is expected to expose, at minimum:
        get_polylines(self)'''
    def __init__(self, *args, **kwargs):
        self._attachments = [{'name': 'in', 'pos': (0,0), 'Q':-1, 'dims': (1,.25)}, {'name': 'out', 'pos': (0,0), 'Q':1, 'dims': (1,.25)}]
    def get_polylines(self):
        return ()
    def get_transformed_bbox(self, mat):
        polylines = self.get_polylines()
        left = float("Inf")
        right = float("-Inf")
        top = float("-Inf")
        bottom = float("Inf")
        
        for polyline in polylines:
            for point in polyline:
                if point[0] < left:
                    left = point[0]
                if point[0] > right:
                    right = point[0]
                if point[1] < bottom:
                    bottom = point[1]
                if point[1] > top:
                    top = point[1]
        return (left, bottom, right, top)
        
    def get_bounding_box(self):
        return self.get_transformed_bbox(np.identity(3))
    def get_attachment_points(self, filters = None):
        if filters is None:
            return self._attachments
        else:
            if callable(filters[0]):
                return filters[0](iterable=self._attachments, **filters[1])
            returns = list(self._attachments)
            for filter_func in filters:
                returns = filter_func[0](iterable=returns, **filter_func[1])
            return returns

def attachment_nearest(point = (0,0)):
    return (sorted,
            {'key': lambda x: np.sqrt((x['pos'][0] - point[0])**2 +
                             (x['pos'][1] - point[1])**2)})
    
def attachment_inlet():
    return (filter, {'function': lambda x: x['Q'] < 0})

def attachment_outlet():
    return (filter, {'function': lambda x: x['Q'] > 0})

def attachment_dims(dims = (1, .125)):
    return (filter, {'function': lambda x: x['dims'] == dims})