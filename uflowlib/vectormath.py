import numpy as np
from numpy import cos, sin

def translation_matrix(x, y, z=0):
    return np.matrix(((1, 0, 0, x),
                     (0, 1, 0, y),
                     (0, 0, 1, z),
                     (0, 0, 0, 1)))
def rotation_matrix(l,m,n, theta):
    return np.matrix(((l*l*(1-cos(theta)) + cos(theta), 
                      m*l*(1-cos(theta)) - n*sin(theta), 
                      n*l*(1-cos(theta)) + m * sin(theta),
                      0),
                     (l*m*(1-cos(theta)) + n*sin(theta),
                      m*m*(1-cos(theta)) + cos(theta),
                      n*m*(1-cos(theta)) - l*sin(theta),
                      0),
                     (l*n*(1-cos(theta)) - m*sin(theta),
                      m*n*(1-cos(theta)) + l*sin(theta),
                      n*n*(1-cos(theta)) + cos(theta),
                      0),
                     (0,0,0,1)))