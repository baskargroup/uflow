#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D disp0;
uniform sampler2D disp1;
uniform float yw;
uniform float zw;
uniform float lv;
uniform float rv;
varying vec3 vertex_normal;
varying vec4 vertex_light_position;
varying vec4 vertex_light_position2;
varying vec4 position;

void main(void)
{
    gl_TexCoord[0] = gl_Vertex;
    vec2 p = gl_Vertex.st;
    p.x = p.x*(rv - lv) + lv;
    p.y = p.y*.97;
	vertex_light_position = gl_LightSource[0].position;
	vertex_light_position2 = gl_LightSource[1].position;
	
	float fac = gl_Vertex.z;

    vec2 uv0 = texture2D(disp0, p).xy;
    
    vec2 uv1 =texture2D(disp1, p).xy;
    vec2 uv = fac*uv1 + (1.-fac)*uv0;
    
    gl_Position = gl_ModelViewProjectionMatrix * (vec4(p, gl_Vertex.zw) + vec4(uv/vec2(yw, zw), 0., 0.));
    position = gl_Position;
    //Compute new normal
    float epsi = .01;
    vec2 epsilon = epsi*cross(gl_Normal, vec3(0.,0.,1.)).xy;
    vec2 tn1 = normalize(epsilon + texture2D(disp0, p + epsilon).xy/vec2(yw, zw) - (uv)/vec2(yw, zw));
    vec2 tn2 = normalize(-epsilon + texture2D(disp0, p - epsilon).xy/vec2(yw, zw) - (uv)/vec2(yw, zw));
    vec3 model_normal0 = -vec3(normalize(tn1 + tn2), 0.);
    
    tn1 = normalize(epsilon + texture2D(disp1, p + epsilon).xy/vec2(yw, zw) - (uv)/vec2(yw, zw));
    tn2 = normalize(-epsilon + texture2D(disp1, p - epsilon).xy/vec2(yw, zw) - (uv)/vec2(yw, zw));
    vec3 model_normal1 = -vec3(normalize(tn1 + tn2), 0.);
    vec3 model_normal = fac*model_normal1 + (1.-fac)*model_normal0;
    vertex_normal = gl_NormalMatrix*model_normal;
  // vertex_normal = gl_NormalMatrix*gl_Normal;
}
