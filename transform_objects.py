try:
	import ConfigParser
except:
	import configparser as ConfigParser
from drawable import PillarMesh, Quad
import os
from pyglet.gl import *
import ctypes
import floattex

TEXTURE_WIDTH = 256
TEXTURE_HEIGHT = 256
WIDTH = 1280
HEIGHT = 720

def pillar_transform(tex_s, disp_map, tex_t, mirror = False, fbo=1, shader_obj = 0):
	"""
	Perform a transformation on tex_s using the advection map in disp_map. Store
	the result in tex_t. If necessary, mirror the result.
	
	The transformation is performed using a framebuffer object. If necessary,
	the fbo used can be overridden with the fbo parameter.
	"""
	renderquad = Quad()
	DIMS = (GLint*4)()
	glGetIntegerv(GL_VIEWPORT, DIMS)
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo)
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, tex_t, 0)
	fb_status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT)
	if fb_status != GL_FRAMEBUFFER_COMPLETE_EXT:
		print( "Framebuffer error: " + filter(lambda x: x[1] == fb_status, locals().items())[0][0])
		print( "Could not compute pillar transformation.")
		return
	glViewport(0, 0, TEXTURE_WIDTH, TEXTURE_HEIGHT)
	glMatrixMode(GL_PROJECTION)
	glPushMatrix()
	glLoadIdentity()
	glOrtho(0, 1, 0, 1, -1, 1)
	glMatrixMode(GL_MODELVIEW)
	glPushMatrix()
	glLoadIdentity()
	glEnable(GL_TEXTURE_2D)
	glActiveTexture(GL_TEXTURE0)
	glBindTexture(GL_TEXTURE_2D, tex_s)
	glActiveTexture(GL_TEXTURE1)
	glBindTexture(GL_TEXTURE_2D, disp_map)
	shader_obj.bind()
	shader_obj.uniformi('xmirror', mirror)
	renderquad.draw()
	shader_obj.unbind()
	glActiveTexture(GL_TEXTURE0)
	glBindTexture(GL_TEXTURE_2D, 0)
	glActiveTexture(GL_TEXTURE1)
	glBindTexture(GL_TEXTURE_2D, 0)
	glPopMatrix()
	glMatrixMode(GL_PROJECTION)
	glPopMatrix()
	glMatrixMode(GL_MODELVIEW)
	WIDTH = DIMS[2]
	HEIGHT = DIMS[3]
	glViewport(0, 0, WIDTH, HEIGHT)
	glDisable(GL_TEXTURE_2D)


#Pillar class, responsible for drawing itself and performing its own transformation.
#One exists for each pillar in the channel.
class Pillar:
	def __init__(self, ptype, state = {}, advection_shader = 0, loc = None, fbo = 1):
		self.ptype = ptype
		self.state = state
		self.loc = loc
		self.hidden = False
		self.fbo = fbo
		self.advection_shader = advection_shader

	def draw(self):
		if not self.hidden:
			if self.loc is not None:
				glPushMatrix()
				glTranslatef(self.loc[0], self.loc[1], self.loc[2])
				self.ptype.draw(self.state)
				glPopMatrix()
			else:
				self.ptype.draw(self.state)

	def transform(self, tex_s, tex_t):
		pillar_transform(tex_s, self.ptype.maps[self.state['d']][abs(self.state['y'])], tex_t, self.state['y'] < 0, self.fbo, self.advection_shader)

	def get_map(self):
		return self.ptype.maps[self.state['d']][abs(self.state['y'])]

#This class holds each pillar type. It's responsible for loading pillar types from files
#and generating the appropriate PillarMesh.
class PillarType:

	def __init__(self, filename):
		conf = ConfigParser.RawConfigParser()
		self.symmetric = False
		try:
			conf.read(filename)
		except:
			return

		try:
			self.desc = conf.get('pillar', 'desc')
		except:
			pass

		try:
			self.params = sorted(map(str.strip, conf.get('pillar', 'params').split(',')))
		except:
			pass

		try:
			self.name = conf.get('pillar', 'name')
			p = list(map(float, conf.get('pillar', 'points').split(',')))
			self.points = list(zip(p[::2], p[1::2]))
			p = list(map(float, conf.get('pillar', 'normals').split(',')))
			self.normals = list(zip(p[::2], p[1::2]))
		except:
			print('Failed to load pillar from ' + filename)
			return

		prefix = ''
		try:
			prefix = conf.get('pillar', 'filename_prefix')
		except:
			pass

		try:
			self.primary_param = conf.get('pillar', 'primary_param').strip()
		except:
			pass

		try:
			self.symmetric = conf.get('pillar', 'symmetric').lower() == 'true'
		except:
			pass
		
		
		
		maps = filter(lambda x: x != 'pillar' and x != 'parameters', conf.sections())
		self.geom = PillarMesh(self.points, self.normals)
		
		for m in maps:
			fn = conf.get(m, 'filename')
			keys = filter(lambda x: x != 'filename', conf.options(m))
			params = dict(zip(keys, map(lambda x: conf.get(m, x), keys)))
			for p in params:
				try:
					params[p] = float(params[p])
				except:
					pass

			if self.primary_param in params:
				if not params[self.primary_param] in self.maps:
					self.maps[params[self.primary_param]] = {}
				curlevel = self.maps[params[self.primary_param]]
				last = curlevel
				for p in sorted(params):
					if p != self.primary_param:
						last = curlevel
						if not params[p] in curlevel:
							curlevel[params[p]] = {}
							curlevel = curlevel[params[p]]
						else:
							curlevel = curlevel[params[p]]
				fname = os.path.join(os.path.split(filename)[0], fn)
				tex = GLuint()
				glGenTextures(1, ctypes.byref(tex))
				f = open(fname, "rb")
				floattex.load_floattex(f, tex)
				f.close()
				last[params[sorted(params)[-1]]] = tex

	def keys(self):
		return self.maps.keys()

	def _draw(self, state):
		glPushMatrix()
		if 'y' in state and state['y'] is not None:
			glTranslatef(state['y'], 0, 0)
		if 'd' in state:
			glScalef(state['d'], state['d'], 1)
		self.geom.draw()
		glPopMatrix()

	def draw(self, state):
		glPushMatrix()
		self._draw(state)
		if self.symmetric and 'y' in state and state['y'] is not None:
			if state['y'] + state['d']/2. > .5:
				glTranslatef(-1, 0, 0)
				self._draw(state)
			if state['y'] - state['d']/2. < -.5:
				glTranslatef(1, 0, 0)
				self._draw(state)
		glPopMatrix()

	name = 'Unnamed Pillar'
	desc = ''
	maps = {}
	geom = None
	params = ['y']
	primary_param = 'y'
