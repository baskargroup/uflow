# coding=utf-8

from widgets import *
import pyglet as pg
import math

from drawable import Cube, Channel, Fiber, Quad, FluidParticles
from uflowlib import advection, ray_march
from uflowlib.renderutil import Camera
import webbrowser

import ctypes
import numpy as np
from scipy import ndimage

try:
    from functools import reduce
except:
    pass

channel = Channel()
cube = Cube(l=0.05, h=0.05, w=0.05)


hover = -1
drag_device = False
raymarcher = ray_march.RaymarchRenderer()
raymarcher.make_shader()
raymarcher.dirty = False

def init_gui(root, program_object):
        program_object.debug_win = gui_toolkit.Window(width = 400, height=300, position=(-1, 20), anchor=(1,0), fill=vec(0,0,0,.1), title="Debug Window")
        program_object.cam = Camera(lookat = (0,0,0), up = (0,0,1), locked = True)

        def debug_update_children(dt):
            i = 0
            win = program_object.debug_win
            clist = list(win.children)
            for child in clist:
                child.deb_time += dt
                i += 1
                if child.deb_time > 5:
                    alpha = 5 - (child.deb_time - 1)
                    if alpha > 0:
                        child.color = vec(1,1,1,alpha)
                    else:
                        win.removeChild(child)
                        alpha = 0
                    i -= 1-alpha
                h = i*15
                child.position = (0, -h)
        program_object.debug_win.tick = debug_update_children
        particles.shader = program_object.shaders['fluid_particle']
        resize_handle = gui_toolkit.ResizeHandle(position = (-1,0), anchor=(1,0), width=20, height=20)
        root.addChild(resize_handle)
        win = gui_toolkit.Window(width = 300, height = 90, resizeable = False, draggable = False, position=(0, -20), anchor=(0, 1))
        win2 = gui_toolkit.Window(width = 300, height = '100%', draggable = False, anchor=(1, 0), position=(300, 0))
        inp = Flow_Input(width=300, height=90, position=(0, 0), program_object = program_object)
        win2.addChild(win)
        win.addChild(inp)
        program_object.inp = inp

        device_output_win = gui_toolkit.Window(width = 310, height = 100, position=(-1,-200), anchor=(1, 1), draggable=True, fill = (0, 0, 0, 0), hover_fill=(0.3, 0.3, 0.3, 0.8))
        program_object.dev_output = Output_Display(width = 300, height = 90, position = (-5,-5), anchor=(1, 1), program_object = program_object)
        device_output_win.addChild(program_object.dev_output)

        program_object.toggle_win2 = True
        
        program_object.win2_slidetime = 1
        def win2_tick(dt):
#            global win2_slidetime
            if program_object.toggle_win2:
                program_object.win2_slidetime += dt*4
            else:
                program_object.win2_slidetime -= dt*4
            if program_object.win2_slidetime > 1:
                program_object.win2_slidetime = 1.
            elif program_object.win2_slidetime < 0:
                program_object.win2_slidetime = 0
            win2.position = (int(300*program_object.win2_slidetime), 0)

        win2.tick = win2_tick


        savepic_label = gui_toolkit.Label(text='SAVE', position=(15,15))

        savepic_button = gui_toolkit.Button(width = "80%", height=45, position=(155,7), anchor=(.5,0))
        savepic_button.addChild(savepic_label)
        if not program_object.have_pil:
            pass
        device_output_win.addChild(savepic_button)
        mouse_previewer_win = gui_toolkit.Window(width=310, height=100, position=(-1,-1), anchor=(1,1), fill=(0,0,0,0))
        program_object.mouse_previewer = Output_Preview(width = 300, height = 90, position = (-5,-5), anchor=(1,1), program_object = program_object)
        mouse_previewer_win.addChild(program_object.mouse_previewer)
        diam_control = gui_toolkit.Window(width=300, height=60, position=(0, -150), anchor=(0,1), fill=(0,0,0,.4))
        program_object.diam_slider = gui_toolkit.SlideContainer(width=250, height=50, continuous = True, range = (.2, .8), legal_values = (.2, .8), position=(25,-30), value=.5)
        diam_l = gui_toolkit.Label(text="<b>Diameter</b>: 0.5", position=(50,-50))
        diam_control.addChild(program_object.diam_slider)
        diam_control.addChild(diam_l)
        win2.addChild(diam_control)

        def update_pillar_ui():
            try:
                program_object.diam_slider.slider.set_value(program_object.curscale)
                program_object.offs_slider.slider.set_value(program_object.pillars[program_object.selection].state['y'])
            except:
                pass

        program_object.set_pillar_ui_update_callback(update_pillar_ui)

        def offs_tick_handler(dt):
            program_object.offs_slider.slider.handled_frame = False
            if program_object.dragging_pillar or program_object.selection >= len(program_object.pillars) and (hover == -1 or hover == len(program_object.pillars)):
                program_object.offs_slider.slider.set_value(program_object.pillar_state['y'])


        def diam_tick_handler(dt):
            program_object.diam_slider.slider.handled_frame = False

        def update_diam_label(slider):
            diam_l.text = "<b>Diameter</b>: " + str(slider.value)
            if not slider.handled_frame:
                if program_object.selection < len(program_object.pillars) and not program_object.dragging_pillar and slider.value != program_object.pillars[program_object.selection].state['d']:
                    program_object.pillar_state = {'d':slider.value, 'y':program_object.pillar_state['y']}
                    program_object.change_pillar(program_object.selection, program_object.curpillar, program_object.pillar_state)
                try:
                    program_object.curscale = slider.value
                except:
                    pass

            slider.handled_frame = True



        offs_control = gui_toolkit.Window(width=300, height=60, position=(0, -220), anchor=(0,1), fill=(0,0,0,.4))
        program_object.offs_slider = gui_toolkit.SlideContainer(width=250, height=50, continuous = True, legal_values = (-.5, .5), range = (-.5, .5), position=(25,-30))
        offs_l = gui_toolkit.Label(text="<b>Offset</b>", position=(50,-50))
        offs_control.addChild(program_object.offs_slider)
        offs_control.addChild(offs_l)
        win2.addChild(offs_control)



        def update_offs_label(slider):
            offs_l.text = "<b>Offset</b>: " + str(slider.value)
            if not slider.handled_frame:
                program_object.pillar_state = {'y':slider.value, 'd':program_object.pillar_state['d']}
                if program_object.selection < len(program_object.pillars) and not program_object.dragging_pillar:
                    program_object.change_pillar(program_object.selection, program_object.curpillar, program_object.pillar_state)
            slider.handled_frame = True


        re_control = gui_toolkit.Window(width=300, height=60, position=(0, -290), anchor=(0,1), fill=(0,0,0,.4))
        program_object.re_slider = gui_toolkit.SlideContainer(width=250, height=50, continuous = False, legal_values = program_object.pcafactory._paramvalues["Re"][2], position=(25,-30), value=20)
        re_l = gui_toolkit.Label(text="<b>Reynolds Number:</b> 20", position=(50,-50))
        re_control.addChild(program_object.re_slider)
        re_control.addChild(re_l)
        win2.addChild(re_control)

        def update_re_label(slider):
            re_l.text = "<b>Reynolds Number</b>: " + str(slider.value)
            program_object._Re = int(slider.value)
            program_object.updatefrom = 0
            program_object._update_maps = True

        pe_control = gui_toolkit.Window(width=300, height=60, position=(0, -360), anchor=(0,1), fill=(0,0,0,.4))
        program_object.pe_slider = gui_toolkit.SlideContainer(width=250, height=50, continuous = False, legal_values = (math.log10(10000.0), math.log10(50000.0), math.log10(100000.0), math.log10(200000.0), math.log10(500000.0), math.log10(10.0**6), math.log10(5*(10.0**6)), math.log10(10.0**7), math.log10(10.0**10)), position=(25,-30), value=5)
#        range = (10**4,10**7),
        pe_l = gui_toolkit.Label(text="<b>Peclet Number</b>: 10e+5", position=(50,-50))
        pe_control.addChild(program_object.pe_slider)
        pe_control.addChild(pe_l)
        win2.addChild(pe_control)

        def update_pe_label(slider):
            pe_l.text = '<b>Peclet Number</b>: {}'.format(10.0**slider.value)
            program_object._Pe = 10.0**slider.value
            #program_object._Pe = slider.value
            program_object.updatefrom = 0

        polym_control = gui_toolkit.Window(width=300, height=60, position=(0, -430), anchor=(0,1), fill=(0,0,0,.4))
        program_object.polym_slider = gui_toolkit.SlideContainer(width=250, height=50, continuous = True, range = (.3, 1), legal_values = (.3, 1), position=(25,-30), value=.5)
        polym_l = gui_toolkit.Label(text="<b>Polymerization Threshold</b>: 0.3", position=(50,-50))
        polym_control.addChild(program_object.polym_slider)
        polym_control.addChild(polym_l)

        def change_polymerization_fraction(slider):
            program_object.polym_threshold = slider.value
            raymarcher.dirty = True
            polym_l.text = "<b>Polymerization Threshold: </b>%.2f"%slider.value

        win2.addChild(polym_control)
        program_object.polym_slider.slider.on_value_changed = change_polymerization_fraction
        program_object.diam_slider.slider.on_value_changed = update_diam_label
        program_object.offs_slider.slider.on_value_changed = update_offs_label
        program_object.offs_slider.slider.tick = offs_tick_handler
        program_object.diam_slider.slider.tick = diam_tick_handler
        program_object.re_slider.slider.on_value_changed = update_re_label
        program_object.pe_slider.slider.on_value_changed = update_pe_label
        program_object.diam_slider.slider.set_value(.5)
        program_object.curscale = .5
        savelabel = gui_toolkit.Label(text='SAVE', position=(15,15))
        save_button = gui_toolkit.Button(width = 80, height=45, position=(150,7), anchor=(.5,0))
        save_button.addChild(savelabel)
        root.addChild(win2)
        win2.addChild(save_button)
        root.addChild(device_output_win)
        newlabel = gui_toolkit.Label(text='NEW', position=(15, 15))
        new_button = gui_toolkit.Button(width = 80, height=45, position=(10,7))
        new_button.addChild(newlabel)
        win2.addChild(new_button)
        loadlabel = gui_toolkit.Label(text='LOAD', position=(15,15))
        load_button = gui_toolkit.Button(width = 80, height=45, position=(-10,7), anchor=(1,0))
        load_button.addChild(loadlabel)
        win2.addChild(load_button)
        swatchpicker = gui_toolkit.Window(width=200, height=35, anchor=(0,1), position=(300,300))
        swatchpicker.index = 2

        def change_color(index, newcolor):
            new_palette = list(program_object.COLOR_PALETTE)
            new_palette[index*3:index*3+3] = newcolor
            program_object.COLOR_PALETTE = new_palette
            inp.refresh_colors(new_palette)
            swatchpicker.hide()
            swatchpicker.disable()
            program_object.updatefrom = 0
            return True

        program_object.change_color = change_color

        def hide_swatch():
            swatchpicker.hide()
            swatchpicker.disable()

        swatchpicker.on_unfocus = hide_swatch
        inp.swatchpicker = swatchpicker

        def gen_swatch_func(swatch, color):
            return lambda: change_color(swatch.index, color)

        for i in range(int(len(COLOR_PALETTE_SW)/3)):
            c = (COLOR_PALETTE_SW[i*3], COLOR_PALETTE_SW[i*3+1], COLOR_PALETTE_SW[i*3+2], 1)
            h_c = tuple(.6*comp + .4 for comp in c)
            swatch = gui_toolkit.Button(width=25, height=25, position=(5+i*32, 5), fill=c, hover_fill=h_c)
            swatch.on_button_press = gen_swatch_func(swatchpicker, c[0:3])
            swatchpicker.addChild(swatch)
            swatchpicker.width = 5+(i+1)*32



        root.addChild(swatchpicker)
        swatchpicker.hide()
        swatchpicker.disable()



        export_button = gui_toolkit.Button(width = 280, height=45, position=(10, 60))
        export_label = gui_toolkit.Label(text="EXPORT DXF", width=100, position=(140, 15), anchor=(.5, 0))
        export_button.addChild(export_label)
        win2.addChild(export_button)

        callbacks = program_object.get_callbacks();

        export_button.on_button_press = callbacks["export"]

        undo_button = gui_toolkit.Button(width = 130, height=45, position= (10, 113))
        undo_text = gui_toolkit.Label(text="UNDO", position=(45,15))
        undo_button.addChild(undo_text)
        win2.addChild(undo_button)


        redo_button = gui_toolkit.Button(width = 130, height=45, position= (-10, 113), anchor=(1,0))
        redo_text = gui_toolkit.Label(text="REDO", position=(45,15))
        redo_button.addChild(redo_text)
        win2.addChild(redo_button)

        def open_update_site():
            url = "http://biomicrofluidics.com/software.php"
            webbrowser.open(url, new=2)

        pillar_label = gui_toolkit.Label(text = b"uFlow 2018".decode("utf-8"), position=(500, 7))
        if program_object.update_available:
            update_label = gui_toolkit.Label(position=(-430,3), text = "A new build (#%d) of uFlow is available at http://biomicrofluidics.com"%program_object.latest_version)

            link = gui_toolkit.Button(width = 170, height=15,position=(650, 26), fill = (0,0,0,0))
            link.addChild(update_label)
            link.on_button_press = open_update_site
            root.addChild(link)
        build_label = gui_toolkit.Label(text = "Build #%d"%program_object.BUILDNUM, position=(-100, 7))
        root.addChild(build_label)
        #pillar_label = gui_toolkit.Label(text = "meh", position=(500, 7))
        root.addChild(pillar_label)

        mouse_follower = gui_toolkit.Window(width = 1, height = 1)
        root.addChild(mouse_follower)

        pillar_debug_label = gui_toolkit.Label(text = "", position = (0, -50))
        mouse_follower.addChild(pillar_debug_label)




        save_button.on_button_press = callbacks["save"]
        load_button.on_button_press = callbacks["load"]
        new_button.on_button_press = callbacks["new"]

        undo_button.on_button_press = callbacks["undo"]
        redo_button.on_button_press = callbacks["redo"]

        undo_button.disable()
        undo_text.color = (.4,.4,.4,1)
        redo_button.disable()
        redo_text.color = (.4,.4,.4,1)

        program_object.undo_button = undo_button
        program_object.undo_text = undo_text
        program_object.redo_button = redo_button
        program_object.redo_text = redo_text

        #load_button.disable()
        #loadlabel.color = (.4,.4,.4,1)
        #save_button.disable()
        #savelabel.color = (.4,.4,.4,1)

        savepic_button.on_button_press = callbacks["save_highres"]

        program_object.null_map = advection.AdvectionMap()
        a = ndimage.imread(program_object.maskfile)[:,:,0]
        raymarcher.set_mask(a)


def register_renderer(window, app):
    app.raymarcher = raymarcher
    @window.event('on_draw')
    def render_screen():
        """Draw the 3D scene."""
        #mouse_follower.position = (mouse_location[0], mouse_location[1])

        global hover

        if app.updatefrom < len(app.pillars):
            app.compute_pillars(app.updatefrom, app.updatefrom + 1)

            raymarcher.dirty = True
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glClear(GL_STENCIL_BUFFER_BIT)

        glDisable(GL_DEPTH_TEST)
        hover = -1
        try:
            hover = filter(lambda x: abs(x - app.cur_spot - app.mouse_location[3]/app.pillar_spacing) <= app.snapdistance, range(len(app.pillars) + 1))[0]
        except:
            pass

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0, 1, 0, 1, -1, 1)
        glMatrixMode(GL_MODELVIEW)
        glPushMatrix()
        glLoadIdentity()
        app.shaders["background"].bind()
        app.shaders["background"].uniformf('time', app.time*3)
        app.shaders["background"].uniformf('shiftx', app.zangle*10)
        app.shaders["background"].uniformf('shifty', -app.xangle*10)
        renderquad.draw()
        app.shaders["background"].unbind()
        glPopMatrix()
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        ASPECT_RATIO = app.ASPECT_RATIO
        if app.camera_style==1:
            glOrtho(-5*ASPECT_RATIO, 5*ASPECT_RATIO, -5, 5, -10, 100)
        elif app.camera_style == 0:
            glOrtho(-5*ASPECT_RATIO, 5*ASPECT_RATIO, -5, 5, -10, 100)
        elif app.camera_style == 2:
            glOrtho(-5*ASPECT_RATIO, 5*ASPECT_RATIO, -5, 5, -10, 100)
        glMatrixMode(GL_MODELVIEW)
        glEnable(GL_DEPTH_TEST)
        glClear(GL_DEPTH_BUFFER_BIT)
        glPushMatrix()
        if app.camera_style==1:
            app.cam.pos = (app.camdistance*math.cos(app.xangle*math.pi/180)*math.sin(app.zangle*math.pi/180), 
                app.camdistance*math.cos(app.xangle*math.pi/180)*math.cos(app.zangle*math.pi/180), 
                app.camdistance*math.sin(app.xangle*math.pi/180))
            glMatrixMode(GL_PROJECTION)
            glLoadIdentity()
            glOrtho(-ASPECT_RATIO*app.camdistance, ASPECT_RATIO*app.camdistance, -app.camdistance, app.camdistance, -10, 100)
            glMatrixMode(GL_MODELVIEW)
        app.cam.transform()
        glGetDoublev(GL_PROJECTION_MATRIX, app.proj_mat)
        glGetIntegerv(GL_VIEWPORT, app.viewport_vec)
        glGetDoublev(GL_MODELVIEW_MATRIX, app.channel_model_mat)
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(1, 1, 1, 0.5))
        glMaterialf(GL_FRONT, GL_SHININESS, 50)
        if app.updatefrom == len(app.pillars) and app.updatefrom > 0:
            app.dev_output.tex = app._flowfields[app.updatefrom-1]
        if app.zangle % 360 > 180:
            glScalef(-1,1,1)
        app.shaders["objects"].bind()
        app.shaders["objects"].uniformi('use_tex', 0)
        channel.draw()
        app.shaders["objects"].unbind()
        if app.zangle % 360 > 180:
            glScalef(-1,1,1)
        glPushMatrix()
        app.cur_spot += (app.target_spot - app.cur_spot) / 10.0
        glTranslatef(0, -app.cur_spot*app.pillar_spacing, 0)
        app.shaders["objects"].bind()
        app.shaders["objects"].uniformi('use_tex', 1)
        if app.camera_style:
            glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(1, 1, 1, 0.5))
        else:
            glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(1, 1, 1, 0.2))
        glMaterialf(GL_FRONT, GL_SHININESS, 50)
        tex = app.black_fltimg
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, tex)
        glActiveTexture(GL_TEXTURE1)
        glEnable(GL_TEXTURE_2D)
        glBindTexture(GL_TEXTURE_2D, app.black_fltimg)
        glPushMatrix()
        glRotatef(90, 1, 0, 0)
        glTranslatef(-0.5, 0.125, app.pillar_spacing / 2.0)
        glScalef(1, 0.125, 1)
        glTranslatef(0,0,0)
        glTranslatef(0,0,-(len(app.pillars)+1)*app.pillar_spacing)
        preview_alpha = .1
        channel_density_mult = 4
        for j, p in enumerate(app.pillars):
            i = len(app.pillars) - j - 1
            glTranslatef(0, 0, app.pillar_spacing)
            try:
                m = app._cumulative_maps[i]
                a = app._flowfields[i]
                glActiveTexture(GL_TEXTURE0)
                a.bind()
                renderquad.draw()
                glScalef(1, -1, 1)
                renderquad.draw()
                glScalef(1, -1, 1)
                a.unbind()
            except:
                pass
        glTranslatef(0, 0, app.pillar_spacing)
        app.null_map.draw(inletstreams = tuple(app.streams), inletcolors = tuple(app.COLOR_PALETTE))
        #particles.draw(0, app.black_fltimg, 1, alpha=preview_alpha, density_multiplier=channel_density_mult)
        glScalef(1, -1, 1)
        app.null_map.draw(inletstreams = tuple(app.streams), inletcolors = tuple(app.COLOR_PALETTE))
        #particles.draw(0, app.black_fltimg, 1, alpha=preview_alpha, density_multiplier=channel_density_mult)

        #app.shaders["fluid_particle"].unbind()


        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D, 0)
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, 0)

        glPopMatrix()
        glDisable(GL_TEXTURE_2D)



        app.shaders["objects"].bind()
        app.shaders["objects"].uniformi('use_tex', 0)

        if app.camera_style:
            glEnable(GL_STENCIL_TEST)
        glStencilFunc(GL_EQUAL, 1, 1)

        glPushMatrix()
        glScalef(.99,.99,.99)
        app.shaders["objects"].bind()
        for i, p in enumerate(app.pillars):
            if i != app.selection and i != hover:
                glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(0.4, 0.4, 0.4, 1))
                glMaterialf(GL_FRONT, GL_SHININESS, 50)
                p.draw()
            elif i == hover and not app.placing_pillar:
                glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(0.5, 0.5, 0.5, 0.6))
                glMaterialf(GL_FRONT, GL_SHININESS, 50)
                p.draw()

            elif not app.placing_pillar:
                glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(0.8, 0.8, 0.4, 0.6 + 0.1 * math.sin(app.time * 5.0)))
                glMaterialf(GL_FRONT, GL_SHININESS, 50)
                p.draw()
            glTranslatef(0, app.pillar_spacing, 0)


        glPushMatrix()

        t = app.check_mask_update()
        if t:
            try:
                a = ndimage.imread(app.maskfile)[:,:,0]
                raymarcher.set_mask(a)
                app.maskupdatetime = t
            except:
                pass

        try:
            glTranslatef(.5, .5, 1)
            glRotatef(90,1,0,0)
            i = len(app.pillars)-1

            if raymarcher.dirty:
                streams = tuple(k for j in ((0,0,1) if i else (0,0,0) for i in app.polymerized_streams) for k in j)
                a = app._cumulative_maps[i].render(inletstreams = tuple(app.streams), inletcolors = streams, sigma=(1.*len(app.pillars)/app._Pe))
                #a = app._flowfields[i].read_pixels()
                result = np.vstack((a[::-1,:,2], a[:,:,2]))
                raymarcher.set_cross_section(result, app.polym_threshold)
                #TBD: Find some way to supply mask
                raymarcher.set_extrude_matrix(np.matrix(np.eye(4)))
                raymarcher.dirty = False

            raymarcher.color = app._flowfields[i]
            #glDisable(GL_DEPTH_TEST)
            glEnable(GL_CULL_FACE)

      #     glStencilFunc(GL_ALWAYS, 0, 1)
       #     glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE)
            glCullFace(GL_BACK)
            raymarcher.draw()
            glDisable(GL_CULL_FACE)
            #glEnable(GL_DEPTH_TEST)
            glStencilFunc(GL_EQUAL, 1, 1)
            glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP)
        except:
            pass
            #This will fail if len(app.pillars) == 0, or
            #if len(app._cumulative_maps) < len(app.pillars), such as
            #when loading a file with more pillars than we had before.
        glPopMatrix()
        #app.shaders["objects"].unbind()
        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D, 0)
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, 0)




        glPopMatrix()


        glPopMatrix()

        app.shaders["objects"].bind()
        if app.curpillar is not None and app.camera_style:
            app.shaders["objects"].uniformi('use_tex', 0)

            curloc = min(app.curys, key=lambda x: abs(x - app.mouse_location[2]))
            ay = -1
            app.placing_pillar = abs(app.selection - app.cur_spot - app.mouse_location[3]/app.pillar_spacing) <= app.snapdistance
            if app.selection < len(app.pillars) and not app.dragging_pillar:
                curloc = app.pillars[app.selection].state['y']
            app.pillar_state = {'d': app.curscale,
                 'y': curloc}
            #app.update_pillar_ui()

            if app.placing_pillar:
                glEnable(GL_STENCIL_TEST)
                glStencilFunc(GL_EQUAL, 1, 1)
                if app.selection == len(app.pillars):
                    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(0.4, 0.4, 0.4, 1))
                else:
                    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(0.5, 0.5, 0.4, 1))

                glMaterialf(GL_FRONT, GL_SHININESS, 50)
                ay = curloc
                glPushMatrix()
                glTranslatef(0, (app.selection - app.cur_spot)*app.pillar_spacing, 0)
                app.pillartypes[app.curpillar].draw(app.pillar_state)
                glPopMatrix()
            elif False:
                glEnable(GL_STENCIL_TEST)
                glStencilFunc(GL_EQUAL, 1, 1)
                glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(1, 0.3, 0.3, 0.6))
                glMaterialf(GL_FRONT, GL_SHININESS, 50)
                app.pillar_state = {'d':app.curscale,
                 'y': None}
                glPushMatrix()
                glTranslatef(app.mouse_location[2], app.mouse_location[3], 0)
                app.pillartypes[app.curpillar].draw(app.pillar_state)


                glPopMatrix()
            #glDisable(GL_DEPTH_TEST)
            for y in app.curys:
                if ay == y:
                    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(1, 1, .4, 1))
                else:
                    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, vec(0, 0.5, 0, 3.))
                glPushMatrix()
                glTranslatef(y, (app.selection - app.cur_spot)*app.pillar_spacing, 0)
                cube.draw()
                glPopMatrix()
            #glEnable(GL_DEPTH_TEST)

        app.shaders["objects"].unbind()
        if len(app.pillars) > 0:
            glLoadIdentity()

        if app.camera_style:
            glDisable(GL_STENCIL_TEST)
        glPopMatrix()
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0,app.w,0,app.h,-1,1)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glDisable(GL_DEPTH_TEST)
        app.root._draw()
        glEnable(GL_DEPTH_TEST)

        loc = (.5 - (- app.cur_spot - app.mouse_location[3]/app.pillar_spacing))
        if loc > len(app.pillars):
            loc = len(app.pillars)-.0000001
        elif loc < 0:
            loc = 0
        t = int(loc)
       # if t >= 0 and len(app.pillars) > 0:
        #    tex = app.pillars[t].out_tex
        #    tex1 = app.pillars[t].in_tex
       # else:
        tex = app.black_fltimg
        tex1 = app.black_fltimg
        glEnable(GL_TEXTURE_2D)

        fac = 1 - loc%1
        app.mouse_previewer.fac = fac
        app.mouse_previewer.tex0 = tex
        app.mouse_previewer.tex1 = tex1

        err = glGetError()
        if err:
            app.debug_print("OpenGL Error: " + filter(lambda x: x[1] == err, locals().items())[0][0] + ' (' + str(err) + ')')


    @window.event
    def on_mouse_motion(mx, my, dx, dy):
        """Mouse motion event handler for main window. This passes the event
        to the root object of the 2D UI system. If it remains unhandled, then process
        the event as an interaction with the 3D space."""


        if not app.root._on_mouse_motion(mx, my, dx, dy):
            app.mouse_location = (mx, my)
            mouse_zplane = app.mouse_unproject(mx, my)
            app.mouse_location = (mx,
             my,
             mouse_zplane[0],
             mouse_zplane[1],
             0)
            if abs(app.mouse_location[2]) <= .75 and abs(len(app.pillars) - app.cur_spot - app.mouse_location[3]/app.pillar_spacing) <= app.snapdistance:
                app.selection = len(app.pillars)

    @window.event
    def on_mouse_scroll(x, y, scroll_x, scroll_y):
        """Mouse scroll handler. If the UI doesn't handle it, interpret as a zoom action
        on the camera."""

        if not app.root._on_mouse_scroll(x,y,scroll_x,scroll_y):
            app.camdistance *= 2**-scroll_y
            if app.camdistance < 1:
                pass
                #app.camdistance = .5

    @window.event
    def on_key_press(symbol, modifiers):
        """Keypress event handler for the application. TODO: reimplement as smaller
        functions."""

        if symbol == pg.window.key.LEFT:
            app.selection -= 1
            app.target_spot -= 1
            if app.selection < 0:
                app.selection = 0
            if app.target_spot < 0:
                app.target_spot = 0
        elif symbol == pg.window.key.V:
#            global toggle_win2
            app.toggle_win2 = not app.toggle_win2
        elif symbol == pg.window.key.Z and modifiers & (pg.window.key.MOD_CTRL):
            app.undo()
        elif symbol == pg.window.key.Y and modifiers & (pg.window.key.MOD_CTRL):
            app.redo()
        elif symbol == pg.window.key.S and modifiers & (pg.window.key.MOD_CTRL):
            #Dump the device.
            app.save_device()
        elif symbol == pg.window.key.F12:
            app.save_highres()
        elif symbol == pg.window.key.O and modifiers & (pg.window.key.MOD_CTRL):
            app.load_device()
        elif symbol == pg.window.key.C:
            app.camera_style += 1
            if app.camera_style > 2:
                app.camera_style = 0
            if app.camera_style==0:
                app.cam.move(pos = (0,-1.5,.125), lookat = (0,1,0), locked = False, up=(0,0,1), time = 1)
            if app.camera_style==1:
                app.cam.move(pos = (10, 0, .125), lookat = (0,0,0), locked = True, up=(0,0,1), time = 1)
            if app.camera_style==2:
                app.cam.move(pos = (0, 0, 10), lookat = (0,0,-10), locked = False, up=(-1,0,0), time = 1)
        elif symbol == pg.window.key.RIGHT:
            app.selection += 1
            app.target_spot += 1
            if app.selection > len(app.pillars)-1:
                app.selection = len(app.pillars)-1
            if app.target_spot > len(app.pillars)-1:
                app.target_spot = len(app.pillars)-1
            if app.selection < 0:
                app.selection = 0
            if app.target_spot < 0:
                app.target_spot = 0
        elif symbol == pg.window.key.ENTER:
            if app.add_pillar(app.pillar_state):
                app.selection += 1
                app.target_spot += 1
        elif symbol == pg.window.key.BACKSPACE:
            if app.selection > 0:
                app.snap_undo()
                app.remove_pillar(app.selection)
        elif symbol == pg.window.key.DELETE:
            if app.selection < len(app.pillars):
                app.snap_undo()
                app.remove_pillar(app.selection+1)
        elif symbol == pg.window.key.BRACKETLEFT:
            app.curpillar -= 1
            app.curpillar = app.curpillar % len(app.pillartypes)
            app.debug_print("Pillar selected: " + str(app.curpillar))
            app.debug_print("\t" + app.pillartypes[app.curpillar].name)
            app.debug_print("\t" + app.pillartypes[app.curpillar].desc)
        elif symbol == pg.window.key.BRACKETRIGHT:
            app.curpillar += 1
            app.curpillar = app.curpillar % len(app.pillartypes)
            app.debug_print("Pillar selected: " + str(app.curpillar))
            app.debug_print("\t" + app.pillartypes[app.curpillar].name)
            app.debug_print("\t" + app.pillartypes[app.curpillar].desc)
        elif symbol == pg.window.key.ESCAPE:
            return pg.event.EVENT_HANDLED


    @window.event
    def on_mouse_drag(mx, my, dx, dy, buttons, mods):
        '''Mouse drag event handler.'''
        global drag_device
        if not app.root._on_mouse_drag(mx,my,dx,dy,buttons,mods):
            app.mouse_location = (mx, my)
            mouse_zplane = app.mouse_unproject(mx, my)
            app.mouse_location = (mx,
             my,
             mouse_zplane[0],
             mouse_zplane[1],
             0)
            if buttons & pg.window.mouse.RIGHT:
                app.xangle += -dy/ 2.0
                if app.xangle < -90:
                    app.xangle = 0-90
                    pass
                if app.xangle > 90:
                    app.xangle = 90
                    pass
                app.zangle += dx / 2.0
            if buttons & pg.window.mouse.LEFT and (mx - app.pressloc[0]) ** 2 + (my - app.pressloc[1]) ** 2 > 4:
                app.pressing = False
                if (app.mouse_location[3] - app.pressloc[3]) ** 2 >= .01 and (app.mouse_location[2] - app.pressloc[2]) ** 2 <= .01 and not app.dragging_pillar:
                    drag_device = True
                elif (app.mouse_location[3] - app.pressloc[3]) ** 2 <= .01 and (app.mouse_location[2] - app.pressloc[2]) ** 2 >= .01 and not drag_device:
                    app.dragging_pillar = True
                if drag_device:
                    m = app.mouse_unproject(mx, my)
                    app.target_spot = app.pressloc[4] + (- m[1] + app.pressloc[3])/app.pillar_spacing
                    app.cur_spot = app.target_spot
                try:
                    app.selection = filter(lambda x: abs(x - app.cur_spot - app.mouse_location[3]/app.pillar_spacing) <= app.snapdistance, range(len(app.pillars) + 1))[0]
                    app.update_pillar_ui()
                except:
                    pass
            if buttons & pg.window.mouse.MIDDLE and (mx - app.pressloc[0]) ** 2 + (my - app.pressloc[1]) ** 2 > 4:
                app.pressing = False
                m = app.mouse_unproject(mx, my)
                app.target_spot = app.pressloc[4] + (- m[1] + app.pressloc[3])/app.pillar_spacing
                app.cur_spot = app.target_spot


    @window.event
    def on_mouse_press(x, y, buttons, mods):
        '''Mouse press event handler.'''

        if not app.root._on_mouse_press(x,y,buttons,mods):
            if buttons & pg.window.mouse.MIDDLE or buttons & pg.window.mouse.LEFT:
                app.pressing = True
                m = app.mouse_unproject(x, y)
                app.pressloc = (x,
                 y,
                 m[0],
                 m[1],
                 app.cur_spot)
                try:
                    app.selection = filter(lambda x: abs(x - app.cur_spot - app.mouse_location[3]/app.pillar_spacing) <= app.snapdistance, range(len(app.pillars) + 1))[0]
                    app.curscale = app.pillars[app.selection].state['d']
                    app.update_pillar_ui()

                except:
                    pass


    @window.event
    def on_mouse_release(x, y, buttons, mods):
        '''Mouse release event handler.'''

        global drag_device
        app.root._on_mouse_release(x,y,buttons,mods)
        if buttons & pg.window.mouse.LEFT and (app.pressing or app.dragging_pillar):
            app.pressing = False
            if app.placing_pillar:
                if app.selection >= len(app.pillars):
                    if app.add_pillar(app.pillar_state):
                        app.selection += 1
                        app.target_spot += 1
                elif app.pillar_state != app.pillars[app.selection].state:
                    app.change_pillar(app.selection, app.curpillar, app.pillar_state)
        app.dragging_pillar = False
        drag_device = False
