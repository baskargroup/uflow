# 2017.04.19 13:53:00 CDT
#Embedded file name: C:\Users\keega\uflow\drawable.py
try:
    import pyglet as pg
    from pyglet.gl import *
except:
    print('Pyglet not found. Please install pyglet before using this program.')
    a = raw_input('>')
    from sys import exit
    exit()

TEXTURE_WIDTH = 256
TEXTURE_HEIGHT = 256

def arange(mi, ma = -1, step = 1):
    """Reimplementation of arange using lists."""
    if ma == -1:
        ma = mi
        mi = 0
    return [ float(i) * step + mi for i in range(int((ma - mi) / step) + 1) ]


class Mesh(object):

    def __init__(self):
        self.batches = []

    def draw(self):
        for batch in self.batches:
            batch.draw()


class Quad(Mesh):

    def __init__(self):
        super(Quad, self).__init__()
        self.batches.append(pg.graphics.Batch())
        self.batches[-1].add(4, GL_QUADS, None, ('v2i', (0, 0, 1, 0, 1, 1, 0, 1)), ('t2f', (0, 0, 1, 0, 1, 1, 0, 1)))


class Channel:

    def __init__(self, loc = (0, 0), width = 1.0, height = 0.25, length = 40.0):
        self.loc = loc
        self.width = width
        self.height = height
        self.length = length
        self.batch = pg.graphics.Batch()
        self.batch.add(4, GL_QUADS, None, ('v3f/static', (-width / 2,
          -length / 2,
          0,
          -width / 2,
          -length / 2,
          height,
          -width / 2,
          length / 2,
          height,
          -width / 2,
          length / 2,
          0)), ('n3f', (1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0)))
        self.f_batch = pg.graphics.Batch()
        self.f_batch.add(4, GL_QUADS, None, ('v3f/static', (width / 2,
          -length / 2,
          0,
          width / 2,
          -length / 2,
          height,
          width / 2,
          length / 2,
          height,
          width / 2,
          length / 2,
          0)), ('n3f', (-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', (-width / 2,
          -length / 2,
          0,
          -width / 2,
          length / 2,
          0,
          width / 2,
          length / 2,
          0,
          width / 2,
          -length / 2,
          0)), ('n3f', (0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1)))

    def draw(self):
        glEnable(GL_STENCIL_TEST)
        glStencilFunc(GL_ALWAYS, 1, 1)
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE)
        self.batch.draw()
        glStencilFunc(GL_ALWAYS, 0, 1)
        self.f_batch.draw()
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP)
        glDisable(GL_STENCIL_TEST)


class Cube:

    def __init__(self, l = 1, w = 1, h = 1):
        self.batch = pg.graphics.Batch()
        self.batch.add(4, GL_QUADS, None, ('v3f/static', (-l / 2,
          -w / 2,
          -h / 2,
          l / 2,
          -w / 2,
          -h / 2,
          l / 2,
          -w / 2,
          h / 2,
          -l / 2,
          -w / 2,
          h / 2)), ('n3f', (0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', (-l / 2,
          w / 2,
          -h / 2,
          -l / 2,
          w / 2,
          h / 2,
          l / 2,
          w / 2,
          h / 2,
          l / 2,
          w / 2,
          -h / 2)), ('n3f', (0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', (-l / 2,
          -w / 2,
          -h / 2,
          -l / 2,
          -w / 2,
          h / 2,
          -l / 2,
          w / 2,
          h / 2,
          -l / 2,
          w / 2,
          -h / 2)), ('n3f', (-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', (l / 2,
          -w / 2,
          -h / 2,
          l / 2,
          w / 2,
          -h / 2,
          l / 2,
          w / 2,
          h / 2,
          l / 2,
          -w / 2,
          h / 2)), ('n3f', (1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', (-l / 2,
          -w / 2,
          h / 2,
          l / 2,
          -w / 2,
          h / 2,
          l / 2,
          w / 2,
          h / 2,
          -l / 2,
          w / 2,
          h / 2)), ('n3f', (0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', (-l / 2,
          -w / 2,
          -h / 2,
          -l / 2,
          w / 2,
          -h / 2,
          l / 2,
          w / 2,
          -h / 2,
          l / 2,
          -w / 2,
          -h / 2)), ('n3f', (0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1)))

    def draw(self):
        self.batch.draw()


class PillarMesh(Mesh):

    def __init__(self, points, normals, h = 0.25):
        self.batches = []
        self.points = points
        self.normals = normals
        self.gen_mesh_extrude(h)

    def gen_mesh_extrude(self, h = 0.25):
        p = [None] * (len(self.points) * 2)
        p[::2] = [ (pt[0], pt[1], 0) for pt in self.points ]
        p[1::2] = [ (pt[0], pt[1], h) for pt in self.points ]
        p.extend(p[:2])
        p = [ dim for pt in p for dim in pt ]
        n = [None] * (len(self.normals) * 2)
        n[::2] = [ (npt[0], npt[1], 0) for npt in self.normals ]
        n[1::2] = [ (npt[0], npt[1], 0) for npt in self.normals ]
        n.extend(n[:2])
        n = [ dim for pt in n for dim in pt ]
        self.batches.append(pg.graphics.Batch())
        self.batches[-1].add(int(len(p) / 3), GL_TRIANGLE_STRIP, None, ('v3f/static', p), ('n3f/static', n))
        p2 = [ dim for pt in self.points for dim in pt + (0,) ]
        self.batches.append(pg.graphics.Batch())
        b_points = self.points
        b_points.reverse()
        p3 = [ dim for pt in b_points for dim in pt + (h,) ]
        self.batches.append(pg.graphics.Batch())
        self.batches[-1].add(int(len(p3) / 3), GL_TRIANGLE_FAN, None, ('v3f/static', p3), ('n3f/static', [0, 0, 1] * int(len(p3) / 3)))

    points = ()


class Fiber(Mesh):

    def __init__(self, xwidth = 30, ywidth = 30):
        self.batches = []
        self.points = []
        self.normals = []
        for i in range(ywidth):
            self.points.append((0, float(i) / ywidth))
            self.normals.append((-1, 0))

        for i in range(xwidth):
            self.points.append((float(i) / xwidth, 1))
            self.normals.append((0, 1))

        for i in range(ywidth):
            self.points.append((1, 1 - float(i) / ywidth))
            self.normals.append((1, 0))

        for i in range(xwidth):
            self.points.append((1 - float(i) / xwidth, 0))
            self.normals.append((0, -1))

        self.gen_mesh_extrude(h=1)

    def gen_mesh_extrude(self, h = 0.25):
        p = [None] * (len(self.points) * 2)
        p[::2] = [ (pt[0], pt[1], 0) for pt in self.points ]
        p[1::2] = [ (pt[0], pt[1], h) for pt in self.points ]
        p.extend(p[:2])
        p = [ dim for pt in p for dim in pt ]
        n = [None] * (len(self.normals) * 2)
        n[::2] = [ (npt[0], npt[1], 0) for npt in self.normals ]
        n[1::2] = [ (npt[0], npt[1], 0) for npt in self.normals ]
        n.extend(n[:2])
        n = [ dim for pt in n for dim in pt ]
        self.batches.append(pg.graphics.Batch())
        self.batches[-1].add(len(p) / 3, GL_TRIANGLE_STRIP, None, ('v3f/static', p), ('n3f/static', n))
        p2 = [ dim for pt in self.points for dim in pt + (0,) ]


class FluidParticles(object):
    """Object for drawing a grid of deformed fluid particles on the screen."""

    def __init__(self, shader = None):
        self.shader = shader
        particle_density = 128.0 / TEXTURE_WIDTH
        v2f = []
        t2f = []
        for y in arange(0, TEXTURE_HEIGHT - 1, 1.0 / particle_density * 8):
            for x in arange(0, TEXTURE_WIDTH - 1, 1.0 / particle_density):
                v2f.extend((float(x) / TEXTURE_WIDTH, float(y) / TEXTURE_HEIGHT))
                t2f.extend((float(x) / TEXTURE_WIDTH, float(y) / TEXTURE_HEIGHT))

        self.particle_list = pg.graphics.vertex_list(int(len(v2f) / 2), ('v2f', v2f), ('t2f', t2f))

    def draw(self, tex0, tex1, fac, alpha = 1, density_multiplier = 1, limits = (0.0, 1.0, 0.0, 1.0), streams = None, pointsize = 1):
        glEnable(GL_TEXTURE_2D)
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, tex0)
        irange = (limits[1] - limits[0], limits[3] - limits[2])
        if self.shader is not None:
            self.shader.bind()
            if streams is not None:
                self.shader.uniformfv('streams', *streams)
            self.shader.uniformf('fac', fac)
            self.shader.uniformf('alpha', alpha)
            self.shader.uniformf('uv_scale', irange[0] / float(density_multiplier), irange[1] / float(density_multiplier))
        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D, tex1)
        glDepthMask(GL_FALSE)
        glPointSize(pointsize)
        glPushMatrix()
        glScalef(irange[0] / float(density_multiplier), irange[1] / float(density_multiplier), 1)
        for j in range(density_multiplier):
            glPushMatrix()
            for i in range(density_multiplier):
                if self.shader is not None:
                    self.shader.uniformf('uv_offset', irange[0] / float(density_multiplier) * i + limits[0], irange[1] / float(density_multiplier) * j + limits[2])
                self.particle_list.draw(GL_POINTS)
                glTranslatef(1, 0, 0)

            glPopMatrix()
            glTranslatef(0, 1, 0)

        glPopMatrix()
        if self.shader is not None:
            self.shader.unbind()
        glBindTexture(GL_TEXTURE_2D, 0)
        glDisable(GL_TEXTURE_2D)
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, 0)
        glDisable(GL_TEXTURE_2D)
        glDepthMask(GL_TRUE)
