# uncompyle6 version 2.9.10
# Python bytecode 2.7 (62211)
# Decompiled from: Python 2.7.12 (default, Nov 19 2016, 06:48:10)
# [GCC 5.4.0 20160609]
# Embedded file name: C:\Users\keega\uflow\uflowlib\shader.py
# Compiled at: 2016-03-23 11:35:32
try:
    print 'loading shader.py'
except:
    import sys
    sys.stdout = open('stdout.txt', 'w')
    sys.stderr = open('stderr.txt', 'w')

from pyglet.gl import *
from ctypes import *
import pyglet as pg

class Shader:
    def __init__(self, vert=[], frag=[], geom=[]):
        self.handle = glCreateProgram()
        self.linked = False
        self.createShader(vert, GL_VERTEX_SHADER)
        self.createShader(frag, GL_FRAGMENT_SHADER)
        self.link()
        self.renderquad = pg.graphics.Batch()
        self.renderquad.add(4, pg.gl.GL_QUADS, None, ('v2i', (0, 0, 1, 0, 1, 1, 0, 1)), ('t2f', (0, 0, 1, 0, 1, 1, 0, 1)))
        return

    def createShader(self, strings, type):
        count = len(strings)
        strings = map(lambda x: x.encode('ascii'), strings)
        if count < 1:
            return
        else:
            shader = glCreateShader(type)
            src = (c_char_p * count)(*strings)
            glShaderSource(shader, count, cast(pointer(src), POINTER(POINTER(c_char))), None)
            glCompileShader(shader)
            temp = c_int(0)
            glGetShaderiv(shader, GL_COMPILE_STATUS, byref(temp))
            if not temp:
                glGetShaderiv(shader, GL_INFO_LOG_LENGTH, byref(temp))
                buffer = create_string_buffer(temp.value)
                glGetShaderInfoLog(shader, temp, None, buffer)
                print buffer.value
            else:
                glAttachShader(self.handle, shader)
            return

    def link(self):
        glLinkProgram(self.handle)
        temp = c_int(0)
        glGetProgramiv(self.handle, GL_LINK_STATUS, byref(temp))
        if not temp:
            glGetProgramiv(self.handle, GL_INFO_LOG_LENGTH, byref(temp))
            buffer = create_string_buffer(temp.value)
            glGetProgramInfoLog(self.handle, temp, None, buffer)
            print buffer.value
        else:
            self.linked = True
        return

    def bind(self):
        glUseProgram(self.handle)

    def unbind(self):
        glUseProgram(0)

    def uniformf(self, name, *vals):
        if len(vals) in range(1, 5):
            {1: glUniform1f,2: glUniform2f,
               3: glUniform3f,
               4: glUniform4f
               }[len(vals)](glGetUniformLocation(self.handle, name.encode('ascii')), *vals)

    def uniformfv(self, name, vsize, *vals):
        if vsize in range(1, 5):
            {1: glUniform1fv,2: glUniform2fv,
               3: glUniform3fv,
               4: glUniform4fv
               }[vsize](glGetUniformLocation(self.handle, name.encode('ascii')), len(vals) / vsize, (GLfloat * len(vals))(*vals))

    def uniformi(self, name, *vals):
        if len(vals) in range(1, 5):
            {1: glUniform1i,2: glUniform2i,
               3: glUniform3i,
               4: glUniform4i
               }[len(vals)](glGetUniformLocation(self.handle, name.encode('ascii')), *vals)

    def uniform_matrixf(self, name, mat):
        loc = glGetUniformLocation(self.handle, name.encode('ascii'))
        glUniformMatrix4fv(loc, 1, False, (c_float * 16)(*mat))

    def draw(self):
        self.renderquad.draw()
# okay decompiling shader.pyc
