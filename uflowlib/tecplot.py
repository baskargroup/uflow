def load_tecplot(filename):
    f = open(filename, "r")
    num_pts = None
    num_elmts = []
    var_names = []
    while num_pts is None:
        l = f.readline()
        if l.startswith("ZONE"):
            num_pts = int(l.split("=")[1].split(",")[0])
            num_elmts = int(l.split("=")[2].split(",")[0])
        if l.startswith("VARIABLES"):
            var_names = map(lambda x: x.strip("\""), l.split("=")[1].split())
    
    #read nodal data
    points = [None]*num_pts
    for i in range(num_pts):
        points[i] = map(float, f.readline().split())
    #read connectivity data
    conn = [None]*num_elmts
    for i in range(num_elmts):
        conn[i] = map(int, f.readline().split())

    
    return {"vars": var_names, "points": points, "nodes": num_pts, "elements": num_elmts, "connectivity": conn }

class OGLTecplotConverter(object):
    '''
    Convert a tecplot file containing a velocity field over an unstructured grid
    into a bunch of OpenGL textures on a Cartesian grid.
    '''


    def __init__(self, params):
        '''
        Constructor
        '''
        