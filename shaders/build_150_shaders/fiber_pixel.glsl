
varying vec4 vertex_light_position;
varying vec4 vertex_light_position2;
varying vec3 vertex_normal;
varying vec4 position;
uniform bool use_tex;
uniform sampler2D tex;

void main(void)
{
	vec3 v_normal = normalize(vertex_normal);
	float f=100.0;
	float n = 0.1;
	float z = (2. * n) / (f + n - gl_FragCoord.z * (f - n));
	float alpha = sqrt(2. - z*6.);
	
	float diffuse_value = abs(dot(v_normal, normalize(vertex_light_position.xyz)))*1.4 +abs(dot(v_normal, normalize(vertex_light_position2.xyz)))/(dot(position - vertex_light_position2, position - vertex_light_position2))*2.4 + .2;
	
	vec4 col;
	if(!use_tex)
	{
		col = gl_FrontMaterial.diffuse*diffuse_value;
	}
	else {
		vec2 uv = gl_TexCoord[0].st;
		col = texture2D(tex, uv);
	}
	//col = vec4(abs(v_normal), 1.);
	gl_FragColor = vec4(col.xyz, gl_FrontMaterial.diffuse.w*alpha);
}
