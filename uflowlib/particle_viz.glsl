#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D pos;
uniform sampler2D flow_field;
uniform sampler2D vel_field;
uniform float zs;
uniform float spacing;
uniform float xw;
uniform float yw;
uniform float zw;

float eps = 0.001;

void main(void)
{
	
	vec2 uv = gl_TexCoord[0].st;
	
	
	vec4 nuv = texture2D(pos, uv) / vec4(xw, yw, zw, 1.0);
		
	
	vec4 col = texture2D(flow_field, uv + nuv.yz);
	gl_FragColor = vec4(col.rgb, 1.0);
	
	if(abs(nuv.x + 200./xw) < eps)
	{
		//Low velocity. Color these green.
		gl_FragColor = vec4(.0, 1., 0., 1.);
	}
	
	if(abs(nuv.x + 150./xw) < eps)
	{
		//Exited domain. Color these red.
		gl_FragColor = vec4(1.0, 0., 0., 1.);
	}
	
	
}
