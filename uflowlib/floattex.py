#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2013 Live session user <ubuntu@ubuntu>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import struct
import array
from pyglet.gl import *

def load_floatimg(f):
	header = struct.unpack('12sIIIII', f.read(32))
	if header[0] != b'KEEGFLT32IMG':
		print(header[0])
		raise TypeError("File is not a valid 32-bit float texture.")
		return
	d = array.array('f')
	d.fromfile(f, header[1]*header[2]*header[3])
	return (header, d)

def load_floattex(f, tex):
	t = load_floatimg(f)
	glBindTexture(GL_TEXTURE_2D, tex)
	type = None
	if t[0][1] == 3:
		type = (GL_RGB32F_ARB, GL_RGB)
	elif t[0][1] == 4:
		type = (GL_RGBA32F_ARB, GL_RGBA)
	elif t[0][1] == 1:
		type = (GL_LUMINANCE32F_ARB, GL_LUMINANCE)
	elif t[0][1] == 2:
		#pyglet doesn't support two-component textures...
		#raise RunTimeError("pyglet doesn't support two-component textures.")
		
		#add 0.0 components.
		l1 = list(t[1])
		l2 = [0]*(len(t[1])/2)
		l3 = l1 + l2
		l3[::3] = l1[::2]
		l3[1::3] = l1[1::2]
		l3[2::3] = l2
		a = array.array('f')
		a.fromlist(l3) 
		t = (t[0], a)
		type = (GL_RGB32F_ARB, GL_RGB)
		
		
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, type[0], t[0][2], t[0][3], 0, type[1], GL_FLOAT, t[1].tostring())

def save_floattex(filename, d):
	'''
	Save a floattex image in my custom format. d is of the same format as that returned by load_floatimg:
	a tuple with two parts. The first is a header that is a tuple containing the following:
	h[0] = 'KEEGFLT32IMG'
	h[1] = depth
	h[2] = width
	h[3] = height
	h[4] = index
	h[5] = total images in sequence
	The last two components may be set to 0 if the image is not part of a sequence.
	The second part of the tuple contains depth*width*height floats packed into a python array.
	'''
	f = open(filename, "wb")
	h = d[0]
	b = d[1]
	f.write(struct.pack("12sIIIII", h[0], h[1], h[2], h[3], h[4], h[5]))
	b.tofile(f)
	f.close()
	
	

def main():
	
	return 0

if __name__ == '__main__':
	main()

