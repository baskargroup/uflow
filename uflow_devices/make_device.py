#!/home/dan/anaconda2/bin/python

import numpy as np
import scipy as sp

import cPickle
import gzip

default_colors = [ (0, 0, 0, 1),(0, 1, 0, 1),  (0, 0, 0, 1), (1, 0, 0, 1),(0, 1, 1, 1), (1, 0, 0, 1),
    (0, 1, 1, 1), (1, 0, 0, 1),(0, 1, 1, 1), (1, 0, 0, 1),]

def data_32_to_D_y(seq32):
    # Convert integer proxy (1-32) valued pillar sequences to (D,y) pillar
    # Sequences

    seq_D_y_h = np.zeros((3,1))

    seq_D_y_h[0] = 0.250 + 0.125 * np.ceil(seq32/8.0)

    seq_D_y_h[1] = (seq32 - (np.ceil(seq32/8.0)-1)*8.0)*0.125 - 0.5

    seq_D_y_h[2] = 1.0

    return seq_D_y_h

def form_inlet(channels, Ny, Nz):
    bins = len(channels)
    inlet = np.zeros(Ny*Nz)
    count = 0
    for j in range(0, Nz):
        for i in range(0,Ny):
            col = int(np.floor((i / float(Ny)) * bins))
            if channels[col] == 1:
                inlet[count] = 1
                count += 1
            else:
                inlet[count] = 0
                count += 1
    inlet[0] = 0

    return inlet

def translate_device_to_uFlow(seq32, inlet, colors=default_colors):
    Np = len(seq32)
    Ns = len(inlet)

    # Translate pillars
    uFlow_pillars = []
    for n in range(0,Np):
        if seq32[n] > 32:
            print "Error. Cannot export half-pillars to uFlow"

        Dy_pillar = data_32_to_D_y(seq32[n]+1)
        uFlow_pillar_dict = dict(y=Dy_pillar[1][0], d=Dy_pillar[0][0])
        uFlow_pillar_entry = ('Circular', uFlow_pillar_dict)
        uFlow_pillars.append(uFlow_pillar_entry)

    # Translate sliders (inlet)
    uFlow_sliders = []
    slider_start = None
    slider_stop = None


    for n in range(0,Ns):
        # Calculate fractional boundaries of channel
        slider_start_tmp = n/float(Ns)
        slider_stop_tmp = (n+1)/float(Ns)

        # See if channel has fluid
        if inlet[n] == 1:
                if slider_start_tmp != slider_stop:
                # If this is a new section of fluid, append starting point to uFlow_sliders
                # and reset slider_stop
                    slider_start = slider_start_tmp
                    slider_stop = slider_stop_tmp

                    uFlow_sliders.append(slider_start)

                    #Check to see if this is the last section of fluid.  If so, cap it off.
                    if n == Ns-1:
                        slider_stop = slider_stop_tmp
                        uFlow_sliders.append(slider_stop)
                        break

                else:
                    # If this is a continuing section of fluid, replace slider_stop
                    slider_stop=slider_stop_tmp
        # If this fluid is an empty channel that stops a previous string of full channels,
        # cap off the previous slider
        elif n != 0 and n != Ns-1 and inlet[n] == 0 and inlet[n-1] == 1:
            slider_stop = slider_start_tmp
            uFlow_sliders.append(slider_stop)
        # If this is the last channel, cap it off.  I think every design needs a slider at
        # 1.0
        elif inlet[n] == 0 and n == Ns-1:
            # Check if previous channel had fluid
            if inlet[n-1] == 1:
                slider_stop = slider_start_tmp
                uFlow_sliders.append(slider_stop)

            slider_stop = slider_stop_tmp
            uFlow_sliders.append(slider_stop)

    uFlow_colors = colors

    uFlow_version = (0, 2)

    uFlow_dev = dict(pillars=uFlow_pillars,sliders=uFlow_sliders,colors=uFlow_colors,version=uFlow_version)

    return uFlow_dev

def uflow_colors(color):
    color_dict = {'black': (0, 0, 0, 1), 'green': (0, 1, 0, 1), 'orange': (1, 0.5, 0, 1)}
    return color_dict[color]

def make_lab_chip_devices():
    inlet_design = [0, 0, 1, 0, 0]
    inlet_colors = [uflow_colors('black'), uflow_colors('green'), uflow_colors('black')]

    lab_chip_designs = [[[2, 4, 4, 2, 19, 19, 19, 20, 20, 23, 23, 23], inlet_design, inlet_colors, 'lab_chip_shift.dev'],
    [[2, 4, 2, 4, 4, 2, 4, 2, 11, 11], inlet_design, inlet_colors, 'lab_chip_av.dev'],
    [[31, 31, 31, 29, 27, 27, 29, 31, 28, 28], inlet_design, inlet_colors, 'lab_chip_enc.dev'],
    [[19, 19, 19, 19, 19, 19, 19], inlet_design, inlet_colors, 'lab_chip_split.dev']]

    for device in lab_chip_designs:
        make_device(device)

def make_nature_devices():
    inlet_design1 = [0, 1, 0, 1, 0, 1, 0]
    inlet_design2 = [0, 0, 1, 0, 1, 0, 1, 0, 0]

    inlet_colors = [uflow_colors('black'), uflow_colors('orange'), uflow_colors('black'),
    uflow_colors('orange'), uflow_colors('black'), uflow_colors('orange'), uflow_colors('black')]

    nature_designs = [[[11], inlet_design1, inlet_colors, 'nature_1_1.dev'],
    [[11, 11], inlet_design1, inlet_colors, 'nature_1_2.dev'],
    [[11, 11, 11, 11], inlet_design1, inlet_colors, 'nature_1_3.dev'],
    [[11, 11, 11, 11, 11, 11, 11, 11], inlet_design1, inlet_colors, 'nature_1_4.dev'],
    [[13], inlet_design2, inlet_colors, 'nature_2_1.dev'],
    [[13, 13], inlet_design2, inlet_colors, 'nature_2_2.dev'],
    [[13, 13, 13, 13], inlet_design2, inlet_colors, 'nature_2_3.dev'],
    [[13, 13, 13, 13, 13, 13, 13, 13], inlet_design2, inlet_colors, 'nature_2_4.dev'],
    [[15], inlet_design1, inlet_colors, 'nature_3_1.dev'],
    [[15, 15], inlet_design1, inlet_colors, 'nature_3_2.dev'],
    [[15, 15, 15, 15], inlet_design1, inlet_colors, 'nature_3_3.dev'],
    [[15, 15, 15, 15, 15, 15, 15, 15], inlet_design1, inlet_colors, 'nature_3_4.dev']]

    for device in nature_designs:
        make_device(device)


def make_device(design):
    print "Making %s"%(design[3])

    uflow_design = translate_device_to_uFlow(design[0], design[1], design[2])

    f = open(design[3], "wb")
    cPickle.dump(uflow_design, f)
    f.close()


def main():
    make_lab_chip_devices()
    make_nature_devices()


if __name__ == '__main__':
    main()
