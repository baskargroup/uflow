import pyglet as pg
from pyglet.gl import *
from drawable import Cube, Channel, Fiber, Quad, FluidParticles
particles = FluidParticles()
from uflowlib import globjects as glo
import math
import gui_toolkit
COLOR_PALETTE_SW = (1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0.5, 0, 0.3, 0.5, 1, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0)
have_pil = False
try:
    from PIL import Image
    have_pil = True
except:
    pass

have_pil = False

def vec(*args):
    """Represent args by array of Glfloats"""
    return (GLfloat * len(args))(*args)


HIGHRES_WIDTH = 1536
HIGHRES_HEIGHT = 1536 / 8
renderquad = Quad()

class Output_Preview(gui_toolkit.Container):

    def __init__(self, *args, **kwargs):
        super(Output_Preview, self).__init__(*args, **kwargs)
        self.fac = 0
        self.program_object = kwargs.get('program_object', None)
        self.tex0 = None

    def draw(self):
        if self.tex0 is not None:
            glPushMatrix()
            glScalef(self._width, self._height / 2.0, 1)
            glTranslatef(0, 1, 0)
            self.tex0.bind()
            self.program_object.shaders['objects'].bind()
            self.program_object.shaders['objects'].uniformi('use_tex', 1)
            renderquad.draw()
            glScalef(1, -1, 1)
            renderquad.draw()
            self.tex0.unbind()
            self.program_object.shaders['objects'].unbind()
            glPopMatrix()


class Output_Display(gui_toolkit.Container):

    def __init__(self, *args, **kwargs):
        super(Output_Display, self).__init__(*args, **kwargs)
        self.expand_parent = False
        self.expand_time = 0
        self.step = 10000
        self.program_object = kwargs.get('program_object', None)
        self.tex = None

    def draw(self):
        if self.expand_parent and self.program_object.highrestex != -1:
            pass
        if self.tex is not None and self.program_object.updatefrom >= len(self.program_object.pillars):
            glPushMatrix()
            glScalef(self._width, self._height / 2.0, 1)
            glTranslatef(0, 1, 0)
            glActiveTexture(GL_TEXTURE0)
            self.tex.bind()
            self.program_object.shaders['objects'].bind()
            self.program_object.shaders['objects'].uniformi('use_tex', 1)
            renderquad.draw()
            glScalef(1, -1, 1)
            renderquad.draw()
            self.tex.unbind()
            self.program_object.shaders['objects'].unbind()
            glPopMatrix()
        elif not self.program_object._update_maps and len(self.program_object.pillars) >= 1:
            glPushMatrix()
            glScalef(self._width, self._height / 2.0, 1)
            glTranslatef(0, 1, 0)
            m = self.program_object._cumulative_maps[-1]
            kwargs = {'inletstreams': tuple(self.program_object.streams),
             'inletcolors': tuple(self.program_object.COLOR_PALETTE),
             'sigma': (1.0 * len(self.program_object.pillars) / self.program_object._Pe)}
            m.draw(**kwargs)
            glScalef(1, -1, 1)
            m.draw(**kwargs)
            glPopMatrix()

    def on_mouse_release(self, x, y, buttons, mods):
        self.expand_parent = not self.expand_parent
        return True

    def on_mouse_press(self, x, y, buttons, mods):
        return True

    def tick(self, dt):
        if self.expand_parent:
            if have_pil or True:
                self.parent.children[1].enable()
            self.parent.hover_fill = None
            if self.expand_time < 1:
                self.expand_time += dt / 0.3
            else:
                self.expand_time = 1
        else:
            if have_pil or True:
                self.parent.children[1].disable()
            if self.expand_time > 0:
                self.expand_time -= dt / 0.3
            else:
                self.expand_time = 0
                self.parent.hover_fill = vec(0.3, 0.3, 0.3, 0.8)
        if self.expand_time <= 1 and self.expand_time >= 0:
            self.expand_time = min(1, max(0, self.expand_time))
            self.parent.width = 310 + (self.get_root()._width - 310) * self.expand_time
            self.parent.height = int(self.parent.width * (100.0 / 310))
            self.height = self.parent.height - 10
            if have_pil or True:
                self.parent.children[1].fill = (0.3,
                 0.3,
                 0.3,
                 self.expand_time * 0.8)
                self.parent.children[1].hover_fill = (0.6,
                 0.6,
                 0.6,
                 self.expand_time * 0.8)
                self.parent.children[1].children[0].color = (1,
                 1,
                 1,
                 self.expand_time * 0.8)
                self.parent.children[1].position = (self.parent.width / 2, self.parent.children[1].position[1])
            self.width = self.parent.width - 10
            self.parent.fill = vec(0.3 - 0.2 * self.expand_time, 0.3 - 0.2 * self.expand_time, 0.3 - 0.2 * self.expand_time, self.expand_time * 0.8)
        if self.step < 16:
            self.step += 1
        self.parent.update()


class Flow_Input(gui_toolkit.Container):

    def __init__(self, *args, **kwargs):
        super(Flow_Input, self).__init__(*args, **kwargs)
        self.dragging = -1
        self.program_object = kwargs.get('program_object', None)
        self.sliders = []
        slider = gui_toolkit.Slider(width=15, height=15, pos_extents=(8, self._width - 8), value=0.4)
        self.sliders.append(slider)
        self.addChild(slider)
        slider.on_value_changed = self.slider_change
        slider = gui_toolkit.Slider(width=15, height=15, pos_extents=(8, self._width - 8), value=0.6)
        self.sliders.append(slider)
        self.addChild(slider)
        slider.on_value_changed = self.slider_change
        app = self.program_object
        self.colors = [ (app.COLOR_PALETTE[s * 3 + 0],
         app.COLOR_PALETTE[s * 3 + 1],
         app.COLOR_PALETTE[s * 3 + 2],
         1) for s in range(int(len(app.COLOR_PALETTE) / 3)) ]
        self.boxes = []
        self.cur_stream = 1
        if len(self.sliders) == 0:
            self.boxes = [(0, 1)]
        else:
            self.boxes.append((0, self.sliders[0].value))
            for i in range(len(self.sliders) - 1):
                self.boxes.append((self.sliders[i].value, self.sliders[i + 1].value))

            self.boxes.append((self.sliders[-1].value, 1))
        self.leftv = self.sliders[0].value
        self.rightv = self.sliders[1].value
        self.program_object.streams = self.get_streams()

    def refresh_colors(self, pallette):
        self.colors = [ (pallette[s * 3 + 0],
         pallette[s * 3 + 1],
         pallette[s * 3 + 2],
         1) for s in range(len(pallette) // 3) ]

    def slider_change(self, slider):
        if slider.value < 0.001 or slider.value > 0.999:
            self.sliders.remove(slider)
            self.removeChild(slider)
        self.sliders.sort(key=lambda x: x.value)
        self.boxes = []
        if len(self.sliders) == 0:
            self.boxes = [(0, 1)]
        else:
            self.boxes.append((0, self.sliders[0].value))
            for i in range(len(self.sliders) - 1):
                self.boxes.append((self.sliders[i].value, self.sliders[i + 1].value))

            self.boxes.append((self.sliders[-1].value, 1))
        self.program_object.streams = self.get_streams()
        self.program_object.updatefrom = 0
        self.program_object.raymarcher.dirty = True

    def get_streams(self):
        s = list(map(lambda x: x.value, self.sliders))
        s.append(1)
        return s

    def on_mouse_press(self, x, y, buttons, mods):
        s = self.get_streams()
        v = (x - 8.0) / (self._width - 16)
        if v < 1:
            i = 0
            while s[i] < v and i < 7:
                i += 1

            if buttons & pg.window.mouse.MIDDLE:
                self.program_object.polymerized_streams[i] = not self.program_object.polymerized_streams[i]
                self.program_object.raymarcher.dirty = True
                return True
            if buttons & pg.window.mouse.RIGHT:
                self.swatchpicker.unhide()
                self.swatchpicker.enable()
                self.swatchpicker.index = i
                p = self.get_global_origin()
                self.swatchpicker.position = (p[0] + x, p[1] + y)
                self.swatchpicker.focus()
                return True

    def add_slider(self, v):
        s = gui_toolkit.Slider(width=15, height=15, pos_extents=(8, self._width - 8), value=v)
        s.on_value_changed = self.slider_change
        self.addChild(s)
        self.sliders.append(s)
        self.slider_change(s)
        return s

    def on_double_click(self, nx, ny, buttons, mods):
        if len(self.sliders) < 7:
            v = (nx - 8.0) / (self._width - 16)
            s = self.add_slider(v)
            s.focus()
            return True
        else:
            return False

    def draw(self):
        glBegin(GL_QUADS)
        for i, b in enumerate(self.boxes):
            glColor4f(*self.colors[i])
            glVertex2f((self._width - 16) * b[0] + 8, 15)
            glVertex2f((self._width - 16) * b[1] + 8, 15)
            glVertex2f((self._width - 16) * b[1] + 8, self._height)
            glVertex2f((self._width - 16) * b[0] + 8, self._height)

        glEnd()
