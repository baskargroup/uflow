import pyglet as pg
from pyglet.gl import *
from shader import Shader
import math
from ctypes import byref, pointer
from uflowlib.renderutil import Camera, Cube
from scipy import ndimage
import numpy as np
from uflowlib import globjects as glo

class RaymarchRenderer(object):
    """Raymarcher."""
    box_src = '''//Distance function for a box with sides s

    float box(vec3 x, vec3 s)
    {
    return length(max(abs(x)-s, .0));
    }'''

    textrusion_src = '''//Distance function for an infinite extrusion of a 2D distance function
    float textrusion(vec3 x, sampler2D tex, vec3 dims)
    {
    float d1 = texture2D(tex, x.xy/dims.xy).r;
    return max(d1, box(x - dims/2., dims/2.1));
    }'''

    plane_src = '''//Distance function for an infinite plane
    float plane( vec3 p, vec4 n )
    {
      // n must be normalized
      return dot(p,n.xyz) + n.w;
    }'''

    normcalc_src = '''//Compute the normal at a given point by the finite difference method.

    vec3 get_normal(const vec3 p)
    {
    vec3 n = vec3( f(p + vec3(eps, 0., 0.)) - f(p - vec3(eps, 0., 0.)),
                   f(p + vec3(0., eps, 0.)) - f(p - vec3(0., eps, 0.)),
                   f(p + vec3(0., 0., eps)) - f(p - vec3(0., 0., eps)));
    return normalize(n);
    }'''

    main_src = ''''void main()
    {
    //Convert screen space into local space.
    vec4 eyepos = (gl_ModelViewProjectionMatrixInverse*vec4(gl_FragCoord.xy,0.,1.));
    vec4 eyedir = (gl_ModelViewProjectionMatrixInverse*vec4(gl_FragCoord.xy,1.,1.)) - eyepos;
    vec3 neye = normalize(eyedir.xyz);

    vec3 pos = position;
    int i =0;
    float minpos = 1e6;

    //Start marching in the direction of the eye ray.
    for(; i < 1000 && ! outside(pos); i++)
    {
        //Step forward equal to the distance from the edge.
        //This guarantees we won't pass through the object.
        vec3 t = neye*f(pos);
        //Keep track of minimum distance passed from object, in case
        //we want to draw an outline.
        minpos = min(minpos, f(pos + t));
        pos += t;
        tif(f(pos) <= 1e-6)
        {
            //We've passed really close, let's call it.
            break;
        }
    }
    //Looks like this ray didn't hit anything.
    if(i == 1000 || outside(pos))
    {
        discard;
        //vec3 col = vec3(.1, .1, .8);
        //gl_FragColor = vec4(col*(max(0., dot(get_normal(pos), normalize(pos - light_pos))) + ambient), 1.);
        float far = gl_DepthRange.far;
        float near = gl_DepthRange.near;
        vec4 p = gl_ModelViewProjectionMatrix*vec4(position-1., 1.);
        float ndc = p.z / p.w;
        float d = (((far - near)*ndc) + near + far) / 2.;
        //gl_FragColor = -vec4((gl_ModelViewProjectionMatrix*vec4(position-1., 1.)).xyz, 1.);
        gl_FragDepth = gl_FragCoord.z;

        gl_FragColor = vec4(1.,1.,1.,1.);
    } else {
    //Color and light the surface using the phong shading model.
    tvec4 col = material(pos);
    '''

    def __init__(self, *args, **kwargs):
        self._debug = kwargs.get('debug', False)
        self.color = kwargs.get('color', None)
        self.rendercube = Cube()
        self.ext0 = kwargs.get('ext0', None)
        self.ext1 = kwargs.get('ext1', None)
        self.ambient = kwargs.get('ambient', 0.3)
        self.shaderobj = None
        return

    def render(self):
        pass

    def make_shader(self):
        self.shader = '''varying vec3 vertex_normal;
varying vec3 position;
        '''
        self.shader += '''uniform sampler2D extrusion1;
uniform sampler2D extrusion2;
uniform sampler2D color;
uniform mat4 ext2mat;

uniform float ambient;

float eps = 1e-2;


const vec3 light_pos = vec3( 0., 0., -1.);
'''
        self.shader += self.box_src
        self.shader += self.textrusion_src
        self.shader += self.plane_src
        self.shader += '''
//First extrusion
float ext1(vec3 x)
{
    return textrusion(x, extrusion1, vec3(.99, .25, .99));
}'''
        self.shader += '''
//Second extrusion
float ext2(vec3 x)
{
    x = (vec4(x, 1.)*ext2mat).xyz;
    return textrusion(x.xzy, extrusion2, vec3(.99, .99, .99));
}'''
        self.shader += '''
//Bounding box (keep shapes bounded within volume, for sanity)
float bbox(vec3 x)
{
    return box(x - vec3(.5), vec3(.49));
}
'''
        self.shader += '''
float f (vec3 x)
{
    float e1 = ext1(x);
    float e2 = ext2(x);
    float bb = bbox(x);
    float mp = plane(x+vec3(0., -.001, .0), vec4(0., 1., 0., 0.));
    return min(mp, min(1e8, max(max(e2, e1), bb)));
}
        '''
        self.shader += '''
vec4 material(vec3 x)
{
    if(x.y > .002)
{
    x = abs(x.xyz/vec3(.99, .125, .99)-vec3(0., 1., 0.));
    return texture2D(color, x.xy);
} else {
    x = (vec4(x, 1.)*ext2mat).xyz;
    float d = texture2D(extrusion2, x.xz/vec2(.99,.99)).r;
    return d > .0001 ? d < .003 ? vec4(0.,0.,0.,1.) : vec4(0.,0.,0.,0.) : vec4(1.,1.,1.,1.);
    }
}
'''
        self.shader += '''
//We want to bail if the ray exits the volume.
bool outside(vec3 x)
{
    return (x.x < 0. || x.y < 0. || x.z < 0. || x.x > 1. || x.y > 1. || x.z > 1.);
}
'''
        self.shader += self.normcalc_src
        self.shader += self.main_src
        self.shader += '''
    //gl_FragColor = vec4(position, 1.);
    if(pos.y > .002)
    {
    gl_FragColor = vec4(col.rgb*(max(0., dot(get_normal(pos), normalize(pos - light_pos))) + ambient), col.a);
    } else {
    gl_FragColor = col;
    }
    //gl_FragDepth = (gl_ModelViewProjectionMatrix*vec4(position-1., 1.)).z;
    //gl_FragDepth = gl_FragDepth -.1;

    float far = gl_DepthRange.far;
    float near = gl_DepthRange.near;
    vec4 p = gl_ModelViewProjectionMatrix*vec4(pos-1., 1.);
    float ndc = p.z / p.w;
    float d = (((far - near)*ndc) + near + far) / 2.;

    gl_FragDepth = d;
    }
}'''
        vert_shader_null = '''varying vec3 vertex_normal;
        varying vec3 position;
        void main()
        {
        position = gl_Vertex.xyz + 1.;
        gl_Position = ftransform();
        gl_TexCoord[0] = gl_MultiTexCoord0;
        vertex_normal = normalize(gl_NormalMatrix * gl_Normal);
        }
        '''
        if self._debug:
            print('Created raymarching shader:')
            print(self.shader)
            print('Compiling...')
        self.shaderobj = Shader([vert_shader_null], [self.shader])
        self.shaderobj.bind()
        self.shaderobj.uniformi('extrusion1', 0)
        self.shaderobj.uniformi('extrusion2', 1)
        self.shaderobj.uniformi('color', 2)
        self.shaderobj.uniformf('ambient', 0.3)
        self.shaderobj.unbind()

    def draw(self, color=None):
        if self.shaderobj is None:
            self.make_shader()
        self.ext0.bind(tex=0)
        self.ext1.bind(tex=1)
        if color is None:
            color = self.color
        color.bind(tex=2)
        self.shaderobj.bind()
        self.rendercube.draw()
        self.shaderobj.unbind()
        self.ext0.unbind(tex=0)
        self.ext1.unbind(tex=1)
        return

    def set_extrude_matrix(self, matrix):
        self.shaderobj.bind()
        self.shaderobj.uniform_matrixf('ext2mat', matrix.flatten().tolist()[0])
        self.shaderobj.unbind()

    def set_cross_section(self, t, threshold=0.3):
        d = np.float32(ndimage.distance_transform_edt(t < threshold) / 800.0)
        d = np.append(d.reshape(d.shape[0], d.shape[1], 1), d.reshape(d.shape[0], d.shape[1], 1), axis=2)
        d = np.append(d, d, axis=2)
        t = glo.Texture(d, dims=(d.shape[1], d.shape[0]))
        self.ext0 = t

    def set_mask(self, m):
        d = np.float32(ndimage.distance_transform_edt(m < 0.5) / 800.0)
        d = np.append(d.reshape(d.shape[0], d.shape[1], 1), d.reshape(d.shape[0], d.shape[1], 1), axis=2)
        d = np.append(d, d, axis=2)
        t = glo.Texture(d, dims=(d.shape[1], d.shape[0]))
        self.ext1 = t


if __name__ == '__main__':
    from uflowlib.vectormath import *
    import uflowlib.advection as advection
    cam = Camera(pos=(2, -10, -5), lookat=(0, 0, 0), up=(0, 1, 0), locked=True)
    cube = Cube()
    time = 0

    def update(dt):
        global time
        time += dt
        cam.update(dt)


    pg.clock.schedule_interval(update, 1.0 / 60.0)
    glEnable(GL_DEPTH_TEST)
    r = RaymarchRenderer(debug=True)
    factory = advection.OGLPCAFactory()
    factory.load('maps/pca')
    map1 = factory.get_map(1074)
    maps = [map1]
    for i in range(10):
        maps.append(maps[-1].append(map1))

    colors = (0, 0, 0, 0, 1, 0, 0, 0, 0)
    inletstreams = (0.3, 0.6)
    img = maps[-1].render(inletcolors=colors, inletstreams=inletstreams, sigma=0.008 * np.sqrt(10))
    result = np.vstack((img[::-1, :, 1], img[:, :, 1]))
    r.set_cross_section(result)
    img = maps[0].render(inletcolors=colors, inletstreams=inletstreams, sigma=0.008 * np.sqrt(10))
    result = np.vstack((img[::-1, :, 1], img[:, :, 1]))
    r.set_mask(result)
    w, h = (
     800, 800)
    win = pg.window.Window(width=w, height=h)
    glViewport(0, 0, w, h)
    r.make_shader()
    shader = r.shaderobj
    glEnable(GL_CULL_FACE)
    glClearColor(0.2, 0.4, 0.5, 1.0)
    glEnable(GL_DEPTH_TEST)

    @win.event
    def on_draw():
        glEnable(GL_TEXTURE_2D)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(-0.7, 0.7, -0.7, 0.7, 0.01, 100)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        cam.pos = (math.sin(time) * 2, 1, math.cos(time) * 2)
        cam.transform()
        glTranslatef(0.5, 0.5, 0.5)
        a = translation_matrix(0.5, 0.5, 0.5)
        a = a * rotation_matrix(0, 1, 0, -time * 0)
        a = a * translation_matrix(-0.5, -0.5, -0.5)
        r.set_extrude_matrix(a)
        r.draw()
        glScalef(0.25, 0.25, 0.25)
        glTranslatef(-1.5, -1.5, -1.5)
        cube.draw()


    pg.app.run()
# okay decompiling ray_march.pyc
