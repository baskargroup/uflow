
header = """999
DXF created by Keegan's Pillar Game
0
SECTION
2
HEADER
9
$ACADVER
1
AC1006
9
$INSBASE
10
0.0
20
0.0
30
0.0
9
$EXTMIN
10
0.0
20
0.0
9
$EXTMAX
10
1000.0
20
1000.0
0
ENDSEC
"""

tables = """0
SECTION
2
TABLES
0
TABLE
2
LTYPE
70
1
0
LTYPE
2
CONTINUOUS
70
64
3
Solid line
72
65
73
0
40
0.000000
0
ENDTAB
0
TABLE
2
LAYER
70
6
0
LAYER
2
1
70
64
62
7
6
CONTINUOUS
0
ENDTAB
0
TABLE
2
STYLE
70
0
0
ENDTAB
0
ENDSEC
"""

blocks = """0
SECTION
2
BLOCKS
0
ENDSEC
"""

entities_preamble = """0
SECTION
2
ENTITIES
"""

eof = """
0
ENDSEC
0
EOF

"""

def assemble(polylines):
	npolylines = []
	for polyline in polylines:
		found = False
		cpolylines = list(npolylines)
		for p in cpolylines:
			if polyline[0][0] == p[0][0] and polyline[0][1] == p[0][1]:
				#First point of this polyline matches first point of another.
				#Reverse this polyline, pop off the last point and append other polyline.
				polyline.reverse()
				polyline.pop()
				polyline.extend(p)
				npolylines.remove(p)
			elif polyline[0][0] == p[-1][0] and polyline[0][1] == p[-1][1]:
				#First point of this polyline matches last point of another.
				#Reverse both polylines, then append the other.
				polyline.reverse()
				p.reverse()
				polyline.pop()
				polyline.extend(p)
				npolylines.remove(p)
			elif polyline[-1][0] == p[0][0] and polyline[-1][1] == p[0][1]:
				#Last point of this polyline matches first point of another.
				#Pop the last point and append the other polyline.
				polyline.pop()
				polyline.extend(p)
				npolylines.remove(p)
			elif polyline[-1][0] == p[-1][0] and polyline[-1][1] == p[-1][1]:
				#Last point of this polyline matches last point of another.
				#Pop last point, reverse other polyline, append.
				polyline.pop()
				p.reverse()
				polyline.extend(p)
				npolylines.remove(p)
			if polyline[0][0] == polyline[-1][0] and polyline[0][1] == polyline[-1][1]:
				#This polyline has same first and last point. Remove the last point.
				polyline.pop()
		npolylines.append(polyline)
	print("Assembled " + str(len(polylines)) + " polylines into " + str(len(npolylines)) + " polylines.")
	return npolylines
				

def export_dxf(filename, polyline_list, pl = False, lw = False):
	f = header + tables + blocks + entities_preamble
	if pl:
		p_list = assemble(polyline_list)
		for polyline in p_list:
			if lw:
				f += "0\nLWPOLYLINE\n100\nAcDbPolyline\n70\n1\n90\n"
				f += str(len(polyline))
				f += "\n"
			else:
				f += "0\nPOLYLINE\n100\nAcDb2DPolyline\n70\n1\n"
			
			for pt in polyline:
				f += "10\n"
				f += str(pt[0])
				f += "\n20\n"
				f += str(pt[1])
				f += "\n"
		#f += "0\AcDbPolyline\n90\n"
		#f += str(len(polyline))
		#f += "\n70
	else:
		
		for polyline in polyline_list:
			last_pt = ()
			for pt in polyline:
				if last_pt == ():
					pass
				else:
					f += "0\nLINE\n8\n1\n10\n"
					f += str(last_pt[0])
					f += "\n20\n"
					f += str(last_pt[1])
					f += "\n11\n"
					f += str(pt[0])
					f += "\n21\n"
					f += str(pt[1])
					f += "\n"
				last_pt = pt
		
	f = f[:-1]
	f += eof
	fn = open(filename, "w")
	fn.write(f)
	fn.close()
