varying vec3 vertex_normal;
varying vec3 position;
uniform mat4 viewmatrix;
uniform vec3 campos;

void main()
{

	gl_Position = ftransform();
	gl_TexCoord[0] = gl_MultiTexCoord0;
	vertex_normal = normalize(gl_NormalMatrix * gl_Normal);
	position = gl_Vertex.xyz + 1.;
}