

import sys
PY3 = sys.version > '3'
if PY3:
    import configparser as ConfigParser
else:
    import ConfigParser

import floattex
import os
import tarfile
from pyglet.gl import *
import pyglet as pg
from shader import Shader
from . import globjects as gl
import numpy as np
import gzip

COLOR_PALETTE = (1,0,0,
                 0,0,1,
                 0,1,1,
                 0,1,0,
                 1,0,1,
                 1,0,0,
                 1,.5,0,
                 .3,.5,1)

advection_render_shader_reverse = '''#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D inlet;
uniform sampler2D disp0;
uniform float sigma;

uniform bool xmirror;

uniform vec3 colors[9];
uniform float streams[9];


/*mat4 gaussm = mat4(0.00000067, 0.00002292, 0.00019117, 0.00038771,
                     0.00002292, 0.00078634, 0.00655965, 0.01330373,
                     0.00019117, 0.00655965, 0.05472157, 0.11098164,
                     0.00038771, 0.01330373, 0.11098164, 0.22508352);*/
                     
uniform float weight[5] = float[](0.2270270270, 0.1945946, 0.1216216216, 0.0540540541, 0.0162162162);


vec4 sample(vec2 p)
{
    vec4 col;
    if(streams[0] >= 0.)
    {

        int stream = 0;
        for(int i=0;streams[stream] <  min(p.x, 1.) && stream < 8;stream++) { }
        col = vec4(colors[stream], 1.);

    }
    else {
        col = texture2D(inlet, p);
    }
    return col;
}

float gaussian(float d, float s)
{
    float pi = 3.141592653;
    //return exp(- d*d / (4.0 * pi * s * s));
    //return pow(4*pi*s*s, 0.5)*exp(- d*d / (4.0 * pi * s * s));
    return exp(- d*d / (2.*s*s));
}

float gaussian2(float d, float s)
{
    float pi = 3.141592653;
    float c = pow(1.0 / (4.0 * pi * s), 0.5);
    float e = -1.0*(d * d) / (4.0 * s);
    return c * exp(e);
}

float gaussf(int i, int j,float nf, float s)
{
    //return gaussm[i][j];
    float fi = float(i)/nf;
    float jf = float(j)/nf;
    return gaussian2(sqrt(fi*fi+jf*jf), s);
}

float cosh(float x)
{
    return (exp(x)+exp(-x))/2.;
}

float rect_calc(vec2 d)
{
    float pi = 3.141592653;
    float AR = 0.25;
    float offset = 0.125;
    float m = 155.;
    float n = 155.;

    vec3 xyz = vec3(0., (d.x/1.), (d.y/8. + offset));
    float u = 0.;
    float coeff = (16 * pow(AR, 2.)) / pow(pi, 4.);
    float num;
    float den;
    for(float i = 1.; i <= n; i += 2.)
    {
        for(float j = 1.; j <= m; j += 2.)
        {
            num = sin(i * pi * xyz.y) * sin(j * pi * ((xyz.z)/AR));
            den = i * j * (pow(AR, 2.) * pow(AR, 2.) + pow(j, 2.));

            u += coeff * (num / den);
        }
    }

    // Convert velocity to time-of-flight
    float L = 2.0;
    float u_mean = 0.0043;
    float u_norm = u/u_mean;
    return L / u_norm;
}

void main()
{
    vec2 uv = gl_TexCoord[0].st;
    if(xmirror)
    {
       uv.x = 1.-uv.x;
    }
    vec2 d = texture2D(disp0, uv).yz * vec2(1.,8.);
    if(xmirror)
    {
        d.x = -d.x;
       uv.x = 1.-uv.x;
    }
    vec2 p = uv + d;

    if(sigma <= 0.)
    {
        gl_FragColor = sample(p);
    } else {
        //Sample
        vec4 c = vec4(0.);
        float Dt = sigma*rect_calc(uv.xy);
        float s = pow(Dt, 0.5);
        float s2 = 1.0; 
        float t = 0.;
        int ni = 8;
        float n = 8.;
       
        for(int ii = 0; ii < ni-1; ii++)
        {
            float i = float(ii);

            for(int jj = 0; jj < ni-1; jj++)
            {
                float j = float(jj);
                t += gaussf(ii,jj,n-1.,s2)*4.;
                c += gaussf(ii,jj,n-1.,s2) * (sample(p + vec2((n-1.-i)*s, (n-1.-j)*s)) + sample(p + vec2(-(n-1.-i)*s, (n-1.-j)*s)) + sample(p + vec2(-(n-1.-i)*s, -(n-1.-j)*s)) + sample(p + vec2((n-1.-i)*s, -(n-1.-j)*s)));
            }

            t += gaussf(ii,ni-1,n-1.,s2)*4.;
            c += gaussf(ii,ni-1,n-1.,s2) * (sample(p + vec2((n-1.-i)*s, 0.)) + sample(p + vec2(-(n-1.-i)*s, 0.))+ sample(p + vec2(0., (n-1.-i)*s))+ sample(p + vec2(0., -(n-1.-i)*s)));
        }

        t += gaussf(ni-1,ni-1,n-1.,s2);
        c += gaussf(ni-1,ni-1,n-1.,s2) * sample(p);
        
        
        //gl_FragColor = c;
        gl_FragColor = c/t;        
        //gl_FragColor = (sigma*rect_calcu(uv.xy))*c/t;
    }
}
'''

advection_render_shader_forward = '''#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D inlet;
uniform sampler2D disp0;
uniform float sigma;

uniform bool xmirror;

uniform vec3 colors[8];
uniform float streams[8];

mat4 gaussm = mat4(0.00000067, 0.00002292, 0.00019117, 0.00038771,
                     0.00002292, 0.00078634, 0.00655965, 0.01330373,
                     0.00019117, 0.00655965, 0.05472157, 0.11098164,
                     0.00038771, 0.01330373, 0.11098164, 0.22508352);


vec4 sample(vec2 p)
{
    vec4 col;
    if(streams[0] >= 0.)
    {

        int stream = 0;
        for(int i=0;streams[stream] <  min(p.x, 1.) && stream < 8;stream++) { }
        col = vec4(colors[stream], 1.);

    }
    else {
        col = texture2D(inlet, p);
    }
    return col;
}

float gaussian(float d, float s)
{
    float pi = 3.141592653;
    //return exp(- d*d / (4.0 * pi * s * s));
    return pow(4*pi*s*s, 0.5)*exp(- d*d / (4.0 * pi * s * s));
}

float gaussf(int i, int j,float nf)
{
    //return gaussm[i][j];
    float fi = float(i)/nf;
    float jf = float(j)/nf;
    return gaussian(sqrt(fi*fi+jf*jf), 1.);
}

float cosh(float x)
{
    return (exp(x)+exp(-x))/2.;
}

float rect_calc(vec2 d)
{
    float pi = 3.141592653;
    float AR = 0.25;
    float offset = 0.125;
    float m = 155.;
    float n = 155.;

    vec3 xyz = vec3(0., (d.x/1.), (d.y/8. + offset));
    float u = 0.;
    float coeff = (16 * pow(AR, 2.)) / pow(pi, 4.);
    float num;
    float den;
    for(float i = 1.; i <= n; i += 2.)
    {
        for(float j = 1.; j <= m; j += 2.)
        {
            num = sin(i * pi * xyz.y) * sin(j * pi * ((xyz.z)/AR));
            den = i * j * (pow(AR, 2.) * pow(AR, 2.) + pow(j, 2.));

            u += coeff * (num / den);
        }
    }
    // Hard-coded normalizing value - Dan
    float L = 10.0;
    float u_mean = 0.0043;
    float u_norm = u/u_mean;
    return L / u_norm;
    //return u/0.0043;
}

void main()
{
    vec2 uv = gl_TexCoord[0].st;

    if(xmirror)
    {
       uv.x = 1.-uv.x;
    }
    vec2 displ = texture2D(disp0, uv).yz;
    vec2 d = displ * vec2(1.,8.);

    if(xmirror)
    {
        d.x = -d.x;
       uv.x = 1.-uv.x;
    }

    vec2 p = uv + d;

    if(sigma <= 0.)
    {
        gl_FragColor = sample(p);
    } else {
        //Sample
        vec4 c = vec4(0.);
        float s = pow(sigma*rect_calc(uv.xy), .5);
        float t = 0.;
        int ni = 8;
        float n = 8.;
        for(int ii = 0; ii < ni-1; ii++)
        {
            float i = float(ii);

            for(int jj = 0; jj < ni-1; jj++)
            {
                float j = float(jj);
                t += gaussf(ii,jj,n-1.)*4.;
                c += gaussf(ii,jj,n-1.) * (sample(p + vec2((n-1.-i)*s, (n-1.-j)*s)) + sample(p + vec2(-(n-1.-i)*s, (n-1.-j)*s)) + sample(p + vec2(-(n-1.-i)*s, -(n-1.-j)*s)) + sample(p + vec2((n-1.-i)*s, -(n-1.-j)*s)));
            }

            t += gaussf(ii,ni-1,n-1.)*4.;
            c += gaussf(ii,ni-1,n-1.) * (sample(p + vec2((n-1.-i)*s, 0.)) + sample(p + vec2(-(n-1.-i)*s, 0.))+ sample(p + vec2(0., (n-1.-i)*s))+ sample(p + vec2(0., -(n-1.-i)*s)));
        }

        t += gaussf(ni-1,ni-1,n-1.);
        c += gaussf(ni-1,ni-1,n-1.) * sample(p);
        gl_FragColor = c/t;
    }
}
'''

advection_append_shader = '''#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D first;
uniform sampler2D second;
uniform bool xmirrorf;
uniform bool xmirrors;

void main()
{
    vec2 uv = gl_TexCoord[0].st;
    if(xmirrors)
    {
       uv.x = 1.-uv.x;
    }
    vec3 d = texture2D(second, uv).xyz;
    if(xmirrors)
    {
       uv.x = 1.-uv.x;
       d.y = -d.y;
    }
    vec2 p = uv + d.yz * vec2(1.,8.);
    if(xmirrorf)
    {
       p.x = 1.-p.x;
    }
    vec3 f = texture2D(first, p).xyz;
    if(xmirrorf)
    {
        f.y = -f.y;
    }
    gl_FragColor = vec4(f + d, 1.);
}
'''

boring_vert_shader = '''
void main()
{
    gl_Position = ftransform();
    gl_TexCoord[0] = gl_MultiTexCoord0;
}'''

class AdvectionMap(object):
    '''
    classdocs
    '''

    def __init__(self, *args, **kwargs):
        '''
        Constructor
        '''
        self._tex = kwargs.get("tex", None)
        if len(args) > 0:
            #TBD: load numpy array into texture
            pass
        if self._tex is None:
            self._tex = gl.Texture(dims = (1,1), channels = 3)
        if isinstance(self._tex, gl.Texture):
            self._textureobj = self._tex
            self._tex = self._textureobj._texindex
            self._linspacenums = self._textureobj.get_dimensions()
        self._mirror = kwargs.get("mirror", False)
        self.renderquad = pg.graphics.Batch()
        self.renderquad.add(4, pg.gl.GL_QUADS, None, ('v2i', (0, 0, 1, 0, 1, 1, 0, 1)), ('t2f', (0, 0, 1, 0, 1, 1, 0, 1)))
        
        self.shaderobj = Shader([boring_vert_shader], [advection_render_shader_reverse])
#        self.shaderobj = Shader([boring_vert_shader], [advection_render_shader_forward])
        self.shaderobj.bind()
        self.shaderobj.uniformi('inlet',0)
        self.shaderobj.uniformi('disp0',1)
        self.shaderobj.uniformf('sigma',-1)
        self.shaderobj.unbind()

        self.appendshaderobj = Shader([boring_vert_shader], [advection_append_shader])
        self.appendshaderobj.bind()
        self.appendshaderobj.uniformi('second',1)
        self.appendshaderobj.uniformi('first',0)
        self.appendshaderobj.unbind()

    def draw(self, inlet = None, inletstreams = (.4, .6, 1, .4, .5, .6, .7,2), inletcolors = COLOR_PALETTE, direction = -1, sigma = -1):
        '''Draw the deformed version of the inlet cross-section resulting
from this advection map. The result is drawn in the current OpenGL context.

TBD: implement forward direction.
        '''
        if inlet is not None:
            glActiveTexture(GL_TEXTURE0)
            try:
                inlet.bind()
            except:
                glBindTexture(GL_TEXTURE_2D, inlet)
            inletstreams = (-1.,)*8
        else:
            glActiveTexture(GL_TEXTURE0)
            glBindTexture(GL_TEXTURE_2D, 0)

        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D, self._tex)

        if len(inletstreams) < 8:
            inletstreams = inletstreams + (1,)*(8-len(inletstreams))
        if len(inletcolors) < len(COLOR_PALETTE):
            inletcolors = inletcolors + (0,)*(len(COLOR_PALETTE)-len(inletcolors))
        #print('mirror = {}'.format(self._mirror))
        self.shaderobj.bind()
        self.shaderobj.uniformfv('streams', 1, *inletstreams)
        self.shaderobj.uniformfv('colors', 3, *inletcolors)
        self.shaderobj.uniformf('sigma', sigma)
        self.shaderobj.uniformi('xmirror',self._mirror)
        self.renderquad.draw()
        self.shaderobj.unbind()
        glBindTexture(GL_TEXTURE_2D, 0)

    def render(self,inlet = None, inletstreams = (.4, .6, 1, .4, .5, .6, .7,2), inletcolors = COLOR_PALETTE, direction = -1, sigma = -1, tex = None):
        tex = self.render_to_texture(inlet, inletstreams, inletcolors, direction, sigma, tex)
        im = tex.read_pixels()
        return im

    def render_to_texture(self,inlet = None, inletstreams = (.4, .6, 1, .4, .5, .6, .7,2), inletcolors = COLOR_PALETTE, direction = -1, sigma = -1, tex = None):
        if tex is None:
            dims = (self._linspacenums[0], self._linspacenums[1])
            tex = gl.Texture(dims = dims, channels = 4)
        fbo = gl.FBO()
        fbo.bind(tex)
        glClear(GL_COLOR_BUFFER_BIT)
        self.draw(inlet = inlet, inletstreams = inletstreams, inletcolors = inletcolors, direction = direction, sigma = sigma)
        fbo.unbind()
        return tex

    def append(self, advection_map):
        #Get the advection map specified by the ith coefficients.
        glDisable(GL_DEPTH_TEST)
        new_tex = gl.Texture(dims = self._linspacenums, channels = 4)
        glDisable(GL_BLEND)

        fbo = gl.FBO()
        fbo.bind(new_tex)
        self.appendshaderobj.bind()
        self.appendshaderobj.uniformi('xmirrors',advection_map._mirror)
        self.appendshaderobj.uniformi('xmirrorf',self._mirror)

        glActiveTexture(GL_TEXTURE0)
        self._textureobj.bind()
        glActiveTexture(GL_TEXTURE1)
        advection_map._textureobj.bind()

        self.renderquad.draw()

        self.appendshaderobj.unbind()
        fbo.unbind()

        glEnable(GL_BLEND)
        glEnable(GL_DEPTH_TEST)

        return AdvectionMap(tex = new_tex)

    def array(self):
        output = (GLfloat * (3*self._linspacenums[0]*self._linspacenums[1]))(0)

        #Get pixels from GPU
        glBindTexture(GL_TEXTURE_2D, self._tex)
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, output)

        #Cast into numpy array
        res = np.frombuffer(output, dtype=np.float32).reshape(self._linspacenums[1], self._linspacenums[0], 3)
        return res

class CPUGZipFactory(object):
    '''A class that reconstructs advection maps from a fixed point representation using
    the CPU.'''
    def __init__(self, *args, **kwargs):
        self._dims = (101, 801)
        self.dy_scale = (-.75, .25)
        self.dz_scale = (-.0657, .0763)

    def load(self, filename):
        if os.path.isdir(filename):
            #Path is a folder that contains gz files
            contents = list(map(lambda x: os.path.join(filename, x), os.listdir(filename)))
            directories = list(filter(os.path.isdir, contents))
            files = list(filter(os.path.isfile, contents))

            dy_coeff_file = [f for f in files if f.endswith("dy.gz")][0]
            dz_coeff_file = [f for f in files if f.endswith("dz.gz")][0]
            paramfile = [f for f in files if f.endswith("parameters.txt")][0]
            self.dyf = gzip.open(dy_coeff_file, "rb")
            self.dzf = gzip.open(dz_coeff_file, "rb")
            pff = open(paramfile, "r")
            self._parse_definition_file(pff)
        self.dy = np.fromstring(self.dyf.read(), dtype='int16').reshape(self._totalparams,self._dims[0], self._dims[1])
        self.dz = np.fromstring(self.dzf.read(), dtype='int16').reshape(self._totalparams,self._dims[0], self._dims[1])

    def _parse_definition_file(self, f):
        conf = ConfigParser.ConfigParser()
        conf.readfp(f)
        self._parameters = list(map(lambda x: x.strip(), conf.get("Parameters", "order").split(",")))
        self._totalparams = int(conf.get("Parameters", "total"))
        self._paramvalues = {}
        counts = 1
        for param in self._parameters:
            n = conf.get(param, "name")
            d = conf.get(param, "desc")
            v = list(map(float, conf.get(param, "values").split(",")))
            self._paramvalues[param] = (n,d,v)
            counts = counts * len(v)
        if counts != self._totalparams:
            print("Warning: number of parameters does not match total")

    def get_job_id(self, parameters, fuzzy = True):
        parameters = dict(parameters)
        if fuzzy:

            for k,v in list(parameters.items()):
                c = self._paramvalues[k][2]
                if not v in c:
                    parameters[k] = min(c, key=lambda x: abs(x - v))
        tc = 1
        id = 0
        for k in self._parameters:
            v = parameters[k]
            #i = tc*len(self._paramvalues[p]["values"])
            id += tc*self._paramvalues[k][2].index(v)
            tc = tc * len(self._paramvalues[k][2])
        return id, parameters

    def get_params_from_id(self, id):
        #TBD
        params = {}
        for p in self._parameters:
            l = len(self._paramvalues[p][2])
            i = int(id%l)
            id = (id - i)/l
            params[p] = self._paramvalues[p][2][i]
        return params

    def get_map(self, i = 0):
        #Get the advection map specified by the ith coefficients.
        a = np.zeros((self._dims[0], self._dims[1], 4), dtype=np.float32)
        a[:,:,1] = (self.dy[i,:,:].astype('float32')+.5)/8191*(self.dy_scale[1]-self.dy_scale[0])+self.dy_scale[0]

        a[:,:,2] = (self.dz[i,:,:].astype('float32')+.5)/8191*(self.dz_scale[1]-self.dz_scale[0])+self.dz_scale[0]
        #a[:,:,1] = np.linspace(0, self.dy_scale[1], self._dims[1])[np.newaxis,:]
        #print(a)

        new_tex = gl.Texture(a, dims=(self._dims[1], self._dims[0]))
        return AdvectionMap(tex = new_tex)


class OGLPCAFactory(object):
    '''A class that reconstructs advection maps from a PCA representation using
    the GPU.'''
    def __init__(self, *args, **kwargs):
        self._construction_shader_src = '''
#ifdef GL_ES
precision highp float;
#endif

uniform vec2 coefficients;
uniform sampler2D component;

void main(void)
{
    vec2 uv = gl_TexCoord[0].st;
    vec4 comp = texture2D(component, uv);
    gl_FragColor = vec4(1., comp.xy * coefficients, 1.);
  //  gl_FragColor = vec4(1., 1., comp.z, 1.);
}
        '''

        self._fbo = gl.FBO()
        self._shaderobj = Shader([boring_vert_shader], [self._construction_shader_src])
        self.renderquad = pg.graphics.Batch()
        self.renderquad.add(4, pg.gl.GL_QUADS, None, ('v2i', (0, 0, 1, 0, 1, 1, 0, 1)), ('t2f', (0, 0, 1, 0, 1, 1, 0, 1)))


    def load(self, filename):
        if os.path.isdir(filename):
            #Path is a folder that contains ft files
            contents = list(map(lambda x: os.path.join(filename, x), os.listdir(filename)))
            directories = list(filter(os.path.isdir, contents))
            files = list(filter(os.path.isfile, contents))

            dy_coeff_file = [f for f in files if f.endswith("dy.txt")][0]
            dz_coeff_file = [f for f in files if f.endswith("dz.txt")][0]
            paramfile = [f for f in files if f.endswith("parameters.txt")][0]
            dyf = open(dy_coeff_file, "r")
            self._dy_coeffs = self._parse_coeff_file(dyf)
            dzf = open(dz_coeff_file, "r")
            self._dz_coeffs = self._parse_coeff_file(dzf)
            pff = open(paramfile, "r")
            self._parse_definition_file(pff)
            if len(directories) > 0:
                #subdirectory probably contains ft files.
                ftfiles = list(map(lambda x: os.path.join(directories[0], x), os.listdir(directories[0])))
                meanf = list(filter(lambda x: x.find("mean") > -1, ftfiles))[0]
                ftfiles.remove(meanf)
                ftfiles = sorted(ftfiles, key = lambda x: int(x.split("_")[-1].split(".")[-2]))
                self._mean = self._load_component(open(meanf, "rb"))
                self._components = list(map(lambda x: self._load_component(open(x, "rb"), name = x), ftfiles))
            else:
                raise NotImplementedError
        elif len(os.path.splitext(filename)[1]) > 0:
            #Let's assume this is a gzipped tarfile.
            tar = tarfile.open(filename)
            self._dy_coeffs = self._parse_coeff_file(tar.extractfile('PCA_mapping_dy.txt'))
            self._dz_coeffs = self._parse_coeff_file(tar.extractfile('PCA_mapping_dz.txt'))
            self._parse_definition_file(tar.extractfile('parameters.txt'))
            self._mean = self._load_component(tar.extractfile('E/dydz_mean.ft'))

            names = tar.getnames()
            cmps = filter(lambda x: x.startswith('E/E_'), names)
            cmps = sorted(cmps, key = lambda x: int(x.split("_")[-1].split(".")[-2]))
            self._components = list(map(lambda x: self._load_component(tar.extractfile(x)), cmps))
        self._dims = self._components[0].get_dimensions()
        if len(self._components) != len(self._dy_coeffs[0]):
            print("Number of coefficients does not match number of components; %d!=%d"%(len(self._dy_coeffs[0]), len(self._components)))

    def _parse_definition_file(self, f):
        conf = ConfigParser.ConfigParser()
        conf.readfp(f)
        self._parameters = list(map(lambda x: x.strip(), conf.get("Parameters", "order").split(",")))
        self._totalparams = int(conf.get("Parameters", "total"))
        self._paramvalues = {}
        counts = 1
        for param in self._parameters:
            n = conf.get(param, "name")
            d = conf.get(param, "desc")
            v = list(map(float, conf.get(param, "values").split(",")))
            self._paramvalues[param] = (n,d,v)
            counts = counts * len(v)
        if counts != self._totalparams:
            print("Warning: number of parameters does not match total")

    def get_job_id(self, parameters, fuzzy = True):
        parameters = dict(parameters)
        if fuzzy:

            for k,v in list(parameters.items()):
                c = self._paramvalues[k][2]
                if not v in c:
                    parameters[k] = min(c, key=lambda x: abs(x - v))
        tc = 1
        id = 0
        for k in self._parameters:
            v = parameters[k]
            #i = tc*len(self._paramvalues[p]["values"])
            id += tc*self._paramvalues[k][2].index(v)
            tc = tc * len(self._paramvalues[k][2])
        return id, parameters

    def get_interpolated_coeffs(self, parameters):
        i,p = self.get_job_id(parameters)
        for k,v in list(parameters.items()):
            if not v == p[k]:
                #Nonmatching parameter; interpolate with nearest points
                d1 = dict(parameters)
                d2 = dict(parameters)
                if p[k] > v:
                    d2[k] = p[k]
                    d1[k] = self._paramvalues[k][2][self._paramvalues[k][2].index(p[k])+1]
                else:
                    d1[k] = p[k]
                    d2[k] =  self._paramvalues[k][2][self._paramvalues[k][2].index(p[k])-1]
                s = (v-d1[k])/(d2[k] - d1[k])
                dy1, dz1 = self.get_interpolated_coeffs(d1)
                dy2, dz2 = self.get_interpolated_coeffs(d2)
                return (s*dy2 + (1.-s)*dy1), (s*dz2 + (1.-s)*dz1)
        return self._dy_coeffs[i], self._dz_coeffs[i]


    def get_interpolated_map(self, parameters):
        #First, get the closest map.
        coeffs = self.get_interpolated_coeffs(parameters)
        return self.get_map(0, dy_coeffs = coeffs[0], dz_coeffs = coeffs[1])



    def get_params_from_id(self, id):
        #TBD
        params = {}
        for p in self._parameters:
            l = len(self._paramvalues[p][2])
            i = int(id%l)
            id = (id - i)/l
            params[p] = self._paramvalues[p][2][i]
        return params

    def _parse_coeff_file(self, f):
        #drop the first two lines
        l1, l2 = f.readline(), f.readline()

        coefs = []
        for line in f:
            c = line.split("\t")[1:-1]
            c[:] = map(float, c)
            coefs.append(c)
        coefs = np.array(coefs)
        return coefs
    def _load_component(self, f, name = ""):
        return gl.Texture.from_file(f, name)

    def get_map(self, i = 0, dy_coeffs = None, dz_coeffs = None, cpu=False):
        #Get the advection map specified by the ith coefficients.
        if not cpu:
            glDisable(GL_DEPTH_TEST)
            new_tex = gl.Texture(dims = self._dims, channels = 4)
            glActiveTexture(GL_TEXTURE0)
            glDisable(GL_BLEND)
            self._fbo.bind(new_tex)
            self._shaderobj.bind()
            self._shaderobj.uniformi("component", 0)
            self._shaderobj.uniformfv("coefficients", 2, 1, 1)
            self._mean.bind()
            self.renderquad.draw()
            glEnable(GL_BLEND)
            glBlendFuncSeparate(GL_ONE, GL_ONE,GL_ONE,GL_ONE) # pre-multiply alpha in the shader
            if dy_coeffs is None:
                for k, coefs in enumerate(zip(self._dy_coeffs[i], self._dz_coeffs[i])):
                    self.__add_component(k, coefs[0], coefs[1])
            elif (i == -1):
                for k, coefs in enumerate(zip(self._dy_coeffs[i], self._dz_coeffs[i])):
                    self.__add_component(0*k, 0*coefs[0], 0*coefs[1])
            else:
                for k, coefs in enumerate(zip(dy_coeffs, dz_coeffs)):
                    self.__add_component(k, coefs[0], coefs[1])
            self._shaderobj.unbind()
            self._fbo.unbind()
            glEnable(GL_DEPTH_TEST)
            glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA,GL_ONE,GL_ZERO)
        else:
            a = np.zeros((self._dims[0], self._dims[1], 4), dtype=np.float32)
            if dy_coeffs is None:
                for k, coefs in enumerate(zip(self._dy_coeffs[i], self._dz_coeffs[i])):
                    print('l')
                    c = np.array((0,coefs[0], coefs[1]))
                    n = self._components[k].read_pixels()
                    a += n*c[np.newaxis, np.newaxis, :]
            else:
                for k, coefs in enumerate(zip(dy_coeffs, dz_coeffs)):
                    print('k')
                    c = np.array((0,coefs[0], coefs[1]))
                    n = self._components[k].read_pixels()
                    a += n*c[np.newaxis, np.newaxis, :]
            new_tex = Texture(a)
        return AdvectionMap(tex = new_tex)
    def get_old_map(self, f, name = ""):
        glDisable(GL_DEPTH_TEST)
        new_tex = gl.Texture.from_file(open(f), name)
        glActiveTexture(GL_TEXTURE0)
        glDisable(GL_BLEND)
        self._fbo.bind(new_tex)
        self._shaderobj.bind()
        self._shaderobj.uniformi("component", 0)
        self._shaderobj.uniformfv("coefficients", 2, 1, 1)
        self.renderquad.draw()
        self._shaderobj.unbind()
        self._fbo.unbind()
        return AdvectionMap(tex = new_tex)


    def __add_component(self, comp, cy, cz):
        #TBD: use bindless textures (is this possible with pyglet?)
        # Yes: use GL_NV_bindless_texture and/or GL_NV_shader_buffer_load

        #For now, we'll accumulate into a texture using additive blend mode
        #Unfortunately, since we have two different coefficients, blending needs
        #to happen in the shader.

        #Will need to test whether this works properly if all relevant textures
        #are 32-bit floats. It's possible we'll get precision issues if the hardware
        #tries to blend as though they are 8-bit.


        #Assume calling function has properly set up state

#         glEnable(GL_BLEND)
#         glBlendFunc(GL_ONE, GL_ONE) # pre-multiply alpha in the shader
#         self._construction_shader.bind()

        self._shaderobj.uniformfv("coefficients", 2, cy, cz)
        self._components[comp].bind()
        self.renderquad.draw()
