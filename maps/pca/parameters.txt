[Parameters]
order = Re, y, D, h
counts = 4, 26, 31, 1
total = 3224

[Re]
name = "Reynolds Number"
desc = "Nondimensional number that governs the flow."
values = 40, 30, 20, 10

[y]
name = "Pillar Offset"
desc = "Offset from channel center, nondimensionalized to channel width."
values =  0.5,  0.48,  0.46,  0.44,  0.42,  0.4 ,  0.38,  0.36,  0.34, 0.32,  0.3 ,  0.28,  0.26,  0.24,  0.22,  0.2 ,  0.18,  0.16, 0.14,  0.12,  0.1 ,  0.08,  0.06,  0.04,  0.02, 0.0

[D]
name = "Pillar diameter"
desc = "Diameter of the pillar, nondimensionalized to channel width."
values = 0.8 ,  0.78,  0.76,  0.74,  0.72,  0.7 ,  0.68,  0.66,  0.64, 0.62,  0.6 ,  0.58,  0.56,  0.54,  0.52,  0.5 ,  0.48,  0.46, 0.44,  0.42,  0.4 ,  0.38,  0.36,  0.34,  0.32,  0.3 ,  0.28, 0.26,  0.24,  0.22,  0.2

[h]
name = "Channel height"
desc = "Height of the channel, nondimensionalized to channel width."
values = .25
