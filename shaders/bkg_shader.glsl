
#ifdef GL_ES
precision highp float;
#endif

uniform vec2 resolution;
uniform float time;
uniform float shiftx;
uniform float shifty;

void main(void)
{
	
   float x = (gl_FragCoord.x + shiftx)*.3;
   float y = (gl_FragCoord.y + shifty)*.3;
   float mov0 = x+y+cos(sin(time/25.)*2.)*100.+sin(x/50.)*1000.;
   float mov1 = y / resolution.y / 0.4 + time/7.5;
   float mov2 = x / resolution.x / 0.4;
   float c1 = abs(sin(mov1+time/25.)/2.+mov2/2.-mov1-mov2+time/25.);
   float c2 = abs(sin(c1+sin(mov0/1000.+time/25.)+sin(y/40.+time/25.)+sin((x+y)/100.)*3.));
   float c3 = abs(sin(c2+cos(mov1+mov2+c2)+cos(mov2)+sin(x/1000.)));
   gl_FragColor = vec4( c3*.02+ .1,c2*.02 + c3*.03 + .1,c1*.01+c3*.02 + .6,.3);
   
   /*
    float t = time / 100.;
   
   	vec2 uv = (gl_FragCoord.xy + vec2(shiftx, shifty)) / resolution.xy;
	float mag = 0.5+0.5*sin(t)*sin(uv.x*4.*sin(t*.06) + time/2.123532)*sin(uv.y*4.*sin(t*.26) + t/2.4125);
	vec3 col = (-pow(mag,4.)+.9) * vec3(.3, .6, 1.);
	gl_FragColor = vec4(col,1.0);
	* */
	/*
	
   float x = gl_FragCoord.x;
   float y = gl_FragCoord.y;
   float mov0 = x+y+cos(sin(time)*2.)*100.+sin(x/100.)*1000.;
   float mov1 = y / resolution.y / 0.2 + time;
   float mov2 = x / resolution.x / 0.2;
   float c1 = abs(sin(mov1+time)/2.+mov2/2.-mov1-mov2+time);
   float c2 = abs(sin(c1+sin(mov0/1000.+time)+sin(y/40.+time)+sin((x+y)/100.)*3.));
   float c3 = abs(sin(c2+cos(mov1+mov2+c2)+cos(mov2)+sin(x/1000.)));
   gl_FragColor = vec4( c1,c2,c3,1.0);
   * */
}
	
