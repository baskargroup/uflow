#!/usr/bin/env python

BUILDNUM = 160

import math
import ctypes
import fnmatch
try:
    import cPickle
except:
    import pickle as cPickle

from shader import Shader
import os
import urllib
import numpy as np
try:
    import pyglet as pg
    from pyglet.gl import *
except:
    print('Pyglet not found. Please install pyglet before using this program.')
    a = raw_input('>')
    from sys import exit
    exit()

have_pil = False
try:
    from PIL import Image
    have_pil = True
except:
    print('PIL not found. PNG export disabled.')

have_tkfiledialog = False
try:
    import tkFileDialog
except:
    try:
        from tkinter import filedialog as tkFileDialog
    except:
        print('Failed to import tkFileDialog. File save/load dialog disabled.')

try:
    import Tkinter as tk
except:
    try:
        import tkinter as tk
    except:
        pass

try:
    tkroot = tk.Tk()
    tkroot.withdraw()
    have_tkfiledialog = True
except:
    print('Failed to import tkFileDialog. File save/load dialog disabled.')

from parts import pillar_part, endcaps
import gen_dxf
from transform_objects import Pillar, PillarType
from widgets import *
import gui
from uflowlib import advection as adv
from uflowlib import globjects as glo
WIDTH = 1024
HEIGHT = 768
TEXTURE_WIDTH = 256
TEXTURE_HEIGHT = 256
drag_device = False
COLOR_PALETTE = (
 1, 1, 0,
 0, 0, 1,
 0, 1, 1,
 0, 1, 0,
 1, 0, 1,
 1, 0, 0,
 1, 0.5, 0,
 0.3, 0.5, 1)

def vec(*args):
    """Represent args by array of Glfloats"""
    return (GLfloat * len(args))(*args)


def pow_two(n):
    """Get next highest power of two."""
    return pow(2, int(math.log(n, 2) + 0.5))


class SplashScreen(object):

    def __init__(self, *args, **kwargs):
        self._window = pg.window.Window(style=pg.window.Window.WINDOW_STYLE_BORDERLESS, width=600, height=400)
        self.splashbkg = pg.image.load('splash.png')
        self.icon = pg.image.load('icon.png')
        self.clear()
        self._window.flip()
        self.clear()
        self._window.flip()
        self._window.set_icon(self.icon)

    def clear(self):
        glMatrixMode(GL_PROJECTION)
        glPushMatrix()
        glLoadIdentity()
        glOrtho(0, 600, 0, 400, -1, 1)
        glMatrixMode(GL_MODELVIEW)
        glPushMatrix()
        glLoadIdentity()
        glClearColor(0, 0, 0, 1)
        glClear(GL_COLOR_BUFFER_BIT)
        self.splashbkg.blit(0, 0)
        glPopMatrix()
        glMatrixMode(GL_PROJECTION)
        glPopMatrix()
        glMatrixMode(GL_MODELVIEW)

    def close(self):
        self._window.close()


splash = SplashScreen()

class MainApp(object):
    """Class for the application. TODO: Implement..."""

    def __init__(self, *args, **kwargs):
        global WIDTH
        global TEXTURE_WIDTH
        global TEXTURE_HEIGHT
        global HEIGHT
        self.BUILDNUM = BUILDNUM
        self.have_pil = have_pil
        print(have_pil)
        try:
            latestf = urllib.urlopen('http://biomicrofluidics.com/uflow_buildnum')
            self.latest_version = int(latestf.read())
            latestf.close()
        except:
            self.latest_version = BUILDNUM

        self.update_available = False
        if self.latest_version > BUILDNUM:
            self.update_available = True
        self.COLOR_PALETTE = COLOR_PALETTE
        self.maskfile = 'mask.png'
        self.maskupdatetime = os.stat(self.maskfile).st_mtime
        self.black_fltimg = GLuint()
        self.black_tex = glo.Texture(dims=(TEXTURE_WIDTH, TEXTURE_HEIGHT), channels=3)
        self.black = (GLfloat * (4 * TEXTURE_WIDTH * TEXTURE_HEIGHT))(0)
        glGenTextures(1, ctypes.byref(self.black_fltimg))
        glBindTexture(GL_TEXTURE_2D, self.black_fltimg)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, TEXTURE_WIDTH, TEXTURE_HEIGHT, 0, GL_RGBA, GL_FLOAT, self.black)
        config = Config(double_buffer=True, stencil_size=8)
        self.window = pg.window.Window(width=WIDTH, height=HEIGHT, resizable=True, config=config)
        self.window.set_caption('uFlow')
        self.icon = pg.image.load('icon.png')
        self.window.set_icon(self.icon)
        WIDTH, HEIGHT = self.window.get_size()
        glViewport(0, 0, WIDTH, HEIGHT)
        self.load_shader_objects()
        self._update_maps = False
        self.root = gui_toolkit.Container(width=WIDTH, height=HEIGHT, window=self.window)
        self.pcafactory = adv.OGLPCAFactory()
        self.pcafactory.load('maps/pca/')
        self._Re = 20
        self.pillartypes = []
        self.pillars = []
        self.undo_states = []
        for rootdir, dirnames, filenames in os.walk('maps'):
            for filename in fnmatch.filter(filenames, '*.def'):
                self.pillartypes.append(PillarType(os.path.join(rootdir, filename)))

        print(self.pillartypes)
        self.curpillar = 0
        self.pillar_scales = self.pcafactory._paramvalues['D'][2]
        self.curscale = 0.5
        self._Pe = 100000
        self.curys = self.pcafactory._paramvalues['y'][2]
        a = list(self.curys)
        a.reverse()
        a.extend([ -b for b in self.curys ])
        self.curys = a
        self.init_gui(self.root)
        self.debug_print(str(self.pillar_scales) + ' pillar scales detected.')
        self.debug_print(str(len(self.pillartypes)) + ' pillar geometries detected.')
        self.time = 0
        self._advectionmaps = []
        self._cumulative_maps = []
        self.selection = 0
        self.highrestex = -1
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        glClearColor(0.4, 0.4, 1, 1)
        self.filetypever = (0, 3)
        self._flowfields = []
        self.snapdistance = 0.45
        self.target_spot = 0
        self.pillar_spacing = 1
        glClearStencil(0)
        self.placing_pillar = False
        self.dragging_pillar = False
        self.drag_device = False
        self.camdistance = 5
        self.pressing = False
        self.pressloc = (0, 0, 0, 0, 0)
        gui.register_renderer(self.window, self)
        self.mouse_location = (0, 0, 0, 0, 0)
        glPushMatrix()
        self.xangle = 30
        self.zangle = 145
        self.channel_model_mat = (GLdouble * 16)()
        self.proj_mat = (GLdouble * 16)()
        self.viewport_vec = (GLint * 4)()
        self.pillar_state = {'y':0.0, 'd':0.5}
        self.cur_spot = 0
        self.target_spot = 0
        self.camera_style = 1
        self.updatefrom = 0
        self.polym_threshold = 0.8
        self.polymerized_streams = [False, True, False, False, False, False, False, False]
        splash.close()

    def check_mask_update(self):
        ntime = os.stat(self.maskfile).st_mtime
        if ntime != self.maskupdatetime:
            return ntime
        return False

    def device_version_supported(self, version):
        return version == (0, 3)

    def set_pillar_ui_update_callback(self, func):
        self.update_pillar_ui = func

    def load_shader_objects(self):
        shader_files = [
         'transform_shader', 'particle_transform_shader',
         'fiber_transform_shader', 'bkg_shader', 'vert_null',
         'draw_object_shader', 'fiber_pixel', 'frag_particle',
         'fiber_transform_shader']
        shader_sources = {}
        for s in shader_files:
            with open('shaders/' + s + '.glsl', 'rb') as f:
                shader_sources[s] = f.read().decode()

        self.shaders = {}
        self.shaders['background'] = Shader(shader_sources['vert_null'], shader_sources['bkg_shader'])
        self.shaders['advection'] = Shader(shader_sources['vert_null'], shader_sources['transform_shader'])
        self.shaders['advection'].bind()
        self.shaders['advection'].uniformi('prev', 0)
        self.shaders['advection'].uniformi('disp', 1)
        self.shaders['advection'].uniformi('xmirror', 0)
        self.shaders['advection'].uniformf('yw', 1)
        self.shaders['advection'].uniformf('zw', 0.125)
        self.shaders['advection'].unbind()
        self.shaders['objects'] = Shader(shader_sources['vert_null'], shader_sources['draw_object_shader'])
        self.shaders['objects'].bind()
        self.shaders['objects'].uniformi('tex', 0)
        self.shaders['objects'].unbind()
        self.shaders['fiber'] = Shader(shader_sources['fiber_transform_shader'], shader_sources['fiber_pixel'])
        self.shaders['fiber'].bind()
        self.shaders['fiber'].uniformi('disp0', 0)
        self.shaders['fiber'].uniformi('disp1', 1)
        self.shaders['fiber'].uniformi('use_tex', 0)
        self.shaders['fiber'].uniformf('lv', 0.4)
        self.shaders['fiber'].uniformf('rv', 0.6)
        self.shaders['fiber'].uniformf('yw', 1)
        self.shaders['fiber'].uniformf('zw', 0.125)
        self.shaders['fiber'].unbind()
        self.shaders['fluid_particle'] = Shader(shader_sources['particle_transform_shader'], shader_sources['frag_particle'])
        self.shaders['fluid_particle'].bind()
        self.shaders['fluid_particle'].uniformf('uv_scale', 1, 1)
        self.shaders['fluid_particle'].uniformf('uv_offset', 0, 0)
        self.shaders['fluid_particle'].uniformi('disp0', 0)
        self.shaders['fluid_particle'].uniformi('disp1', 1)
        self.shaders['fluid_particle'].uniformf('fac', 0)
        self.shaders['fluid_particle'].uniformf('yw', 1)
        self.shaders['fluid_particle'].uniformf('zw', 0.125)
        self.shaders['fluid_particle'].uniformfv('streams', 1, 0.4, 0.6, 1, 0.4, 0.5, 0.6, 0.7, 1)
        self.shaders['fluid_particle'].uniformfv('colors', 3, *COLOR_PALETTE)
        self.shaders['fluid_particle'].uniformf('alpha', 1)
        self.shaders['fluid_particle'].unbind()
        return self.shaders

    def debug_print(self, text):
        print(text)
        deb_lab = gui_toolkit.Label(text=text)
        deb_lab.deb_time = 0.0
        self.debug_win.addChild(deb_lab)

    def init_gui(self, root):
        gui.init_gui(root, self)

    def save_highres(self):
        res = self._flowfields[-1].read_pixels()
        th, tw = res.shape[0:2]
        extensions = []
        defaultextension = '.csv'
        if self.have_pil or True:
            extensions.append(('Portable Network Graphics', '*.png'))
            defaultextension = '.png'
        extensions.append(('Comma-separated Value', '*.csv'))
        if have_tkfiledialog:
            filename = tkFileDialog.asksaveasfilename(defaultextension=defaultextension, filetypes=extensions)
        if filename != '' and filename != () and filename[-3:].lower() == 'png':
            app.debug_print('Saving ' + str(tw) + 'x' + str(th) + ' render to ' + filename + '...')
            # im_top = Image.frombuffer('RGBA', (tw, th), (res * 255).astype(np.uint8))
            im_bottom = Image.frombuffer('RGBA', (tw, th), (res * 255).astype(np.uint8), 'raw', 'RGBA', 0, 1)
            im_top = im_bottom.transpose(Image.FLIP_TOP_BOTTOM)
            im = Image.new("RGB",(tw, 2*th))
            im.paste(im_top, (0,0))
            im.paste(im_bottom, (0,th))
            im.save(filename)
        if filename != '' and filename != () and filename[-3:].lower() == 'csv':
            app.debug_print('Exporting CSV file: ' + filename)
            self.save_transformation_csv(filename, self._advectionmaps[-1])

    def remove_pillar(self, sel):
        if sel > 0 and sel <= len(self.pillars):
            self.pillars.pop(sel - 1)
            self._advectionmaps.pop(sel - 1)
            self._cumulative_maps.pop(sel - 1)
            self._flowfields.pop(sel - 1)
            self.selection -= 1
            app.target_spot -= 1
            self.updatefrom = self.selection - 2
            if self.updatefrom < 0:
                self.updatefrom = 0

    def get_callbacks(self):
        return {'export': self.export_dxf,'undo': self.undo,'redo': self.redo,'save': self.save_device,
           'load': self.load_device,'new': self.new_device,'save_highres': self.save_highres
           }

    def export_dxf(self):
        filename = 'device.dxf'
        if have_tkfiledialog:
            filename = tkFileDialog.asksaveasfilename(defaultextension='.dxf', filetypes=[('Autodesk DXF', '*.dxf')])
        if filename != '' and filename != ():
            parts = []
            start = endcaps.Start_Endcap()
            parts.append(start)
            for p in self.pillars:
                pillar = pillar_part.Pillar_Part()
                pillar.set_args(Offset=p.state['y'], Diameter=p.state['d'])
                pillar.attach(parts[-1], parts[-1].get_attachment_points()[0])
                parts.append(pillar)

            end = endcaps.Endcap()
            end.attach(parts[-1], parts[-1].get_attachment_points()[0])
            parts.append(end)
            polylines = []
            for p in parts:
                polylines.extend(p.get_transformed_polylines())

            gen_dxf.export_dxf(filename, polylines)

    def undo(self):
        if len(self.undo_states) > 0:
            self.redo_states.append(list(self.pillars))
            self.redo_text.color = (1, 1, 1, 1)
            self.redo_button.enable()
            self.pillars = self.undo_states.pop()
            self.updatefrom = 0
            self._update_maps = True
        if len(self.undo_states) == 0:
            self.undo_text.color = (0.4, 0.4, 0.4, 1)
            self.undo_button.disable()

    def redo(self):
        if len(self.redo_states) > 0:
            r = self.redo_states
            self.snap_undo()
            self.redo_states = r
            self.pillars = self.redo_states.pop()
            self.updatefrom = 0
            self._update_maps = True
        if len(self.redo_states) == 0:
            self.redo_text.color = (0.4, 0.4, 0.4, 1)
            self.redo_button.disable()
        else:
            self.redo_text.color = (1, 1, 1, 1)
            self.redo_button.enable()

    def save_device(self):
        filename = 'pillar_proto.dev'
        if have_tkfiledialog:
            filename = tkFileDialog.asksaveasfilename(defaultextension='.dev', filetypes=[('Pillar Device', '*.dev')])
        if filename != '' and filename != ():
            f = open(filename, 'wb')
            pils = map(lambda x: (x.ptype.name, x.state), self.pillars)
            colors = tuple(((self.COLOR_PALETTE[i * 3], self.COLOR_PALETTE[i * 3 + 1], self.COLOR_PALETTE[i * 3 + 2]) for i in range(len(self.COLOR_PALETTE) / 3)))
            sdev = {'version': self.filetypever,'pillars': pils,'sliders': self.streams,'colors': colors}
            cPickle.dump(sdev, f)
            f.close()
            self.debug_print('Device saved to file ' + filename + '.')
            self.debug_print('Device has:')
            for i, p in enumerate(self.pillars):
                self.debug_print('\t' + str(i) + ': ' + str(p.state))

    def load_device(self):
        #filename = 'pillar_proto.dev'
        filename = ''
        if have_tkfiledialog:
            filename = tkFileDialog.askopenfilename(defaultextension='.dev', filetypes=[('Pillar Device', '*.dev')])
        if filename != '' and filename is not ():
            f = open(filename, 'rb')
            newdev = cPickle.load(f)
            try:
                version = newdev['version']
            except:
                app.debug_print('No version information. Assuming old device filetype.')
                pillars2 = newdev
                version = (0, 0)

            app.debug_print('File format version ' + '.'.join(map(str, version)) + '.')
            if cmp_version(version, self.filetypever) == 1:
                app.debug_print('WARNING: Device file has higher version')
                app.debug_print('number than is supported. Please update')
                app.debug_print('this program.')
            if cmp_version(version, (0, 1)) >= 0:
                pillars2 = newdev['pillars']
            if cmp_version(version, (0, 2)) >= 0:
                sliders = newdev['sliders']
                sliders2 = list(self.inp.sliders)
                for s in sliders2:
                    self.inp.sliders.remove(s)
                    self.inp.removeChild(s)

                for v in sliders[:-1]:
                    self.inp.add_slider(v)

                cols = newdev['colors']
                for i, col in enumerate(cols):
                    self.change_color(i, col)

            f.close()
            self.new_device()
            self.updatefrom = 0
            self.pillars = []
            for p in pillars2:
                ptype = tuple(filter(lambda x: x.name == p[0], self.pillartypes))[0]
                app.add_pillar(p[1], ptype)

            app.debug_print('Device loaded from ' + filename + '.')
            app.debug_print('Device has:')
            for i, p in enumerate(self.pillars):
                app.debug_print('\t' + str(i) + ': ' + str(p.state))

            self.undo_states = []
            self.redo_states = []

    def new_device(self):
        global undo_states
        global redo_states
        self.pillars = []
        self._advectionmaps = []
        self._flowfields = []
        self._cumulative_maps = []
        undo_states = []
        redo_states = []
        self.updatefrom = 0
        self.selection = 0
        app.target_spot = 0

    def add_pillar(self, state, ptype=None):
        """Add a pillar to the current device. The state parameter should be a dictionary
        of parameters for the pillar object. Right now, that means a diameter and y offset."""
        self.snap_undo()
        if ptype is None:
            ptype = self.pillartypes[self.curpillar]
        if self.curpillar is not None:
            pass
        else:
            return False
        for v in state:
            if state[v] is None:
                return False

        self.pillars.append(Pillar(ptype, state=state))
        p = self.pillars[-1]
        #j, truth = self.pcafactory.get_job_id({'Re': self._Re,'y': abs(p.state['y']),'D': p.state['d'],'h': 0.25})
        m = self.pcafactory.get_interpolated_map({'Re': self._Re,'y': abs(p.state['y']),'D': p.state['d'],'h': 0.25})
        m._mirror = p.state['y'] < 0
        self._advectionmaps.append(m)
        self._update_maps = True
        return True

    def change_pillar(self, id, pillar, state):
        """Change the 'id'th pillar to pillar type 'pillar' with state 'state'."""
        for v in state:
            if state[v] is None:
                return

        if self.pillars[id].state != state:
            self.snap_undo()
            self.pillars[id] = Pillar(self.pillartypes[pillar], state=state)
            #j, truth = self.pcafactory.get_job_id({'Re': self._Re,'y': abs(state['y']),'D': state['d'],'h': 0.25})
            m = self.pcafactory.get_interpolated_map({'Re': self._Re,'y': abs(p.state['y']),'D': p.state['d'],'h': 0.25})
            m._mirror = state['y'] < 0
            self._advectionmaps[id] = m
        if self.updatefrom > id:
            self.updatefrom = id
        return

    def save_transformation_csv(self, filename, tex, matlab_compat=True):
        res = tex.array()
        ydisp = res[0::3]
        zdisp = res[1::3]
        xdisp = res[2::3]
        import csv
        with open(filename, 'w') as csvfile:
            writer = csv.writer(csvfile)
            if not matlab_compat:
                writer.writerow(['CSV Exported from uFlow.'])
                writer.writerow(['Dimensions: ', len(ydisp), len(zdisp)])
            for i in range(h):
                writer.writerow(ydisp[i * w:(i + 1) * w - 1])

            for i in range(h):
                writer.writerow(zdisp[i * w:(i + 1) * w - 1])

    def compute_pillars(self, from_pillar, to_pillar):
        for i in range(from_pillar, to_pillar):
            if self._update_maps:
                p = self.pillars[i]
                j, truth = self.pcafactory.get_job_id({'Re': self._Re,'y': abs(p.state['y']),'D': p.state['d'],'h': 0.25})
                m = self.pcafactory.get_interpolated_map({'Re': self._Re,'y': abs(p.state['y']),'D': p.state['d'],'h': 0.25})
                m._mirror = p.state['y'] < 0
                try:
                    self._advectionmaps[i] = m
                except IndexError:
                    self._advectionmaps.append(m)

            if i > 0:
                flowfield = self._advectionmaps[i].render_to_texture(self._flowfields[i - 1], sigma=(1.0/self._Pe))
                m = self._cumulative_maps[i - 1].append(self._advectionmaps[i])
            elif i == 0:
                flowfield = self._advectionmaps[i].render_to_texture(inletstreams=tuple(self.streams), inletcolors=self.COLOR_PALETTE, sigma=(1.0/self._Pe))
                m = self._advectionmaps[i]
            if len(self._flowfields) <= i:
                self._flowfields.append(flowfield)
                self._cumulative_maps.append(m)
            else:
                self._flowfields[i] = flowfield
                self._cumulative_maps[i] = m

        if self.updatefrom == from_pillar:
            self.updatefrom = to_pillar
        if self.updatefrom == len(self.pillars):
            self._update_maps = False

    def mouse_unproject(self, mx, my):
        """Convert screen-space mouse coordinates to locations on the z=0 plane, using the
        global matrices:
            -channel_model_mat
            -proj_mat
            -viewport_vec
        These matrices must be set properly when this function is called."""
        X = ctypes.c_double()
        Y = ctypes.c_double()
        Z = ctypes.c_double()
        X2 = ctypes.c_double()
        Y2 = ctypes.c_double()
        Z2 = ctypes.c_double()
        try:
            res = gluUnProject(ctypes.c_double(mx), ctypes.c_double(my), ctypes.c_double(0), self.channel_model_mat, self.proj_mat, self.viewport_vec, ctypes.byref(X), ctypes.byref(Y), ctypes.byref(Z))
            res = gluUnProject(ctypes.c_double(mx), ctypes.c_double(my), ctypes.c_double(1), self.channel_model_mat, self.proj_mat, self.viewport_vec, ctypes.byref(X2), ctypes.byref(Y2), ctypes.byref(Z2))
            x = Z.value / (Z.value - Z2.value) * (X2.value - X.value) + X.value
            y = Z.value / (Z.value - Z2.value) * (Y2.value - Y.value) + Y.value
            return (
             x, y)
        except:
            return (0, 0)

    def snap_undo(self):
        self.undo_states.append(list(app.pillars))
        self.undo_text.color = (1, 1, 1, 1)
        self.undo_button.enable()
        self.redo_states = []
        self.redo_text.color = (0.4, 0.4, 0.4, 1)
        self.redo_button.disable()


app = MainApp()
try:
    from itertools import izip_longest as zip_longest
except:
    from itertools import zip_longest as zip_longest

def cmp_version(v1, v2):
    for a, b in zip_longest(v1, v2):
        if a is None:
            a = 0
        if b is None:
            b = 0
        if a > b:
            print("return 1")
            return 1
        if a < b:
            print("return -1")
            return -1

    print("return 0")
    return 0


ASPECT_RATIO = 1.0

@app.window.event
def on_resize(width, height):
    app.debug_print('Viewport resized:' + str(width) + ',' + str(height))
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glViewport(0, 0, width, height)
    app.root.width = width
    app.root.height = height
    app.root.update()
    ASPECT_RATIO = 1.0 * width / height
    glGetDoublev(GL_PROJECTION_MATRIX, app.proj_mat)
    glGetIntegerv(GL_VIEWPORT, app.viewport_vec)
    glMatrixMode(GL_MODELVIEW)
    glGetDoublev(GL_MODELVIEW_MATRIX, app.channel_model_mat)
    app.shaders['background'].bind()
    app.shaders['background'].uniformf('resolution', width, height)
    app.shaders['background'].unbind()
    w = width
    h = height
    app.ASPECT_RATIO = ASPECT_RATIO
    app.w, app.h = w, h
    return pg.event.EVENT_HANDLED


def update(dt):
    app.time = app.time + dt
    app.cam.update(dt)
    app.root._tick(dt)


pg.clock.schedule_interval(update, 1.0 / 60.0)

def main():
    pg.app.run()
    return 0


if __name__ == '__main__':
    main()
# global drag_device ## Warning: Unused global
# okay decompiling uflow.pyc
